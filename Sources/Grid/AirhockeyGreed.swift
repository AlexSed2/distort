public protocol Tailable {
    init(diagonal: SIMD2<Double>, center: SIMD2<Double>, height: Double, zoffset: Float, dummyUV: Bool)
}


public struct AirhockeyGreed<Coub: Tailable> {
    let coubDiagonal: SIMD2<Double>
    let height: Double
    public init(coubDiagonal: SIMD2<Double>, height: Double) {
        self.coubDiagonal = coubDiagonal
        self.height = height
    }
    /// make 12 coubs
    public func makeCoubs4x3(zoffset: Bool = false, dummyUV: Bool) -> [Coub] {
        var centers = [SIMD2<Double>]()
        let topleftx = (coubDiagonal.x / 2) * 3 * -1
        let toplefty = coubDiagonal.y
        var current: SIMD2<Double> = [topleftx, toplefty]
        
        
        while current.y >= -toplefty {
            while current.x <= -topleftx {
                centers.append(current)
                current.x += coubDiagonal.x
            } 
            current.x = topleftx
            current.y -= coubDiagonal.y
        }
        
        let offset: Float = zoffset ? Float(height / 2) : 0
        
        let coubs = centers.map { Coub(diagonal: coubDiagonal * 0.995, center: $0, height: height, zoffset: offset, dummyUV: dummyUV) }
        return coubs
    }
    public func makeCoubs3x3(zoffset: Bool = false, dummyUV: Bool) -> [Coub] {
        var centers = [SIMD2<Double>]()
        let topleftx = (coubDiagonal.x / 2) * 2 * -1
        let toplefty = coubDiagonal.y
        var current: SIMD2<Double> = [topleftx, toplefty]
        
        
        while current.y >= -toplefty {
            while current.x <= -topleftx {
                centers.append(current)
                current.x += coubDiagonal.x
            } 
            current.x = topleftx
            current.y -= coubDiagonal.y
        }
        let offset: Float = zoffset ? Float(height / 2) : 0
        let coubs = centers.map { Coub(diagonal: coubDiagonal * 0.995, center: $0, height: height, zoffset: offset, dummyUV: dummyUV) }
        return coubs
    }
    
    public func makeCoubs2x2(zoffset: Bool = false, centeroffset: SIMD2<Double> = .zero, dummyUV: Bool) -> [Coub] {
        var centers = [SIMD2<Double>]()
        let topleftx = (coubDiagonal.x / 2) * -1
        let toplefty = coubDiagonal.y / 2
        var current: SIMD2<Double> = [topleftx, toplefty]
        
        
        while current.y >= -toplefty {
            while current.x <= -topleftx {
                centers.append(current)
                current.x += coubDiagonal.x
            } 
            current.x = topleftx
            current.y -= coubDiagonal.y
        }
        let offset: Float = zoffset ? Float(height / 2) : 0
        let coubs = centers.map { Coub(diagonal: coubDiagonal * 0.995, center: $0 + centeroffset, height: height, zoffset: offset, dummyUV: dummyUV) }
        return coubs
    }
}
