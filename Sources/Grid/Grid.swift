import simd

public protocol GreedableBody {
    init(position: SIMD2<Double>, diagonal: SIMD2<Double>)
}

public struct ArrangeOnGrid<Body: GreedableBody> {
    public init() {}
    public func arrange(planeDiagonal: SIMD2<Double>, parts: Int) -> [Body] {
        let slots = PlaneGrid(diagonal: planeDiagonal, parts: parts)
        return slots.getLeftSlots().map { Body(position: $0.position, diagonal: $0.diagonal) }
    }
}

/// Center [0, 0, 0]
public struct PlaneGrid {
    public struct Slot: Equatable, Hashable {
        public let position: SIMD2<Double>
        public let diagonal: SIMD2<Double>
        public var positionf: SIMD2<Float> { [Float(position.x), Float(position.y)] }
    }
    let diagonal: SIMD2<Double>
    let parts: Int 
    public init(diagonal: SIMD2<Double>, parts: Int) {
        self.diagonal = diagonal
        self.parts = parts
    }
    func makeSlots() -> [Slot] {
        let horisontalpartscount = parts
        let parts = stride(from: 0.0, to: 1.0, by: 1.0 / Double(horisontalpartscount)).dropFirst()
        let crosses = parts.map { simd_mix([0, 0], diagonal, [$0, $0]) }
        let leftbottomcenter = crosses.first! / 2
        let row = (1..<(horisontalpartscount * 2)).map { SIMD2<Double>(leftbottomcenter.x, 0) * Double($0) }
        let rows = (1..<(horisontalpartscount * 2)).map { integer -> [SIMD2<Double>] in
            return row.map { cell -> SIMD2<Double> in 
                return cell + SIMD2<Double>(0, leftbottomcenter.y) * Double(integer)
            }
        } 
        let twod = rows.map { z -> [Slot] in
            z.map { center -> Slot in 
                Slot(position: center - diagonal / 2, diagonal: crosses.first!)
            }
        }
        
        return twod.reduce(into: [Slot]()) { (result, oned) in
            result.append(contentsOf: oned)
        }
    }
    public func getAllSlots() -> [Slot] {
        makeSlots()
    }
    public func getLeftSlots() -> [Slot] {
        makeSlots().filter { $0.position.x < 0 }
    }
    public func getRightSlotsCenter() -> [Slot] {
        makeSlots().filter { $0.position.x >= 0 }
    }
    public func getRightSlots() -> [Slot] {
        makeSlots().filter { $0.position.x > 0 }
    }
}

public struct TestGrid {
    public init() {}
    public func test() {
//        let grid = PlaneGrid(diagonal: [6, 5], parts: 3)
//        grid.makeSlots()
    }
}
