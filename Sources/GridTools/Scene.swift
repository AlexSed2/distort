#if !os(iOS)
import App

struct Scene {
    func run() {
        runApp()
    }
    func runApp() {
        let packet = Setup().prepareDraw()
        App(packet: packet).run()        
    }
    func runTest() {
        _ = Math().getN(a: 10, b: 5)
    }
}
#endif
