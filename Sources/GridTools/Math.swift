struct Math {
    
    // https://cloud.mail.ru/public/3qH9/4esGcZy3B
    
    func getN(a: Float, b: Float) -> Float {        
        let m = a / 2
        let n: Float
        if b <= m {
            n = b / m
        }  else {
            let t = a - b
            n = t / m
        }
        return n
    }
    
}
