#if !os(iOS)
import MetalKit
import Draw
import App

@available(OSX 10.11, *)
public struct Setup {
    
    public init() {}
    
    public func prepareDraw() -> ControllerPacket {
        
        
        
        /// Вызывается в методе вью контроллера
        func initialdraw() -> (viewsWindowOne: [NSView], viewsWindowTwo: [NSView], viewsWindowThree: [NSView], stored: [Any]) {
            
            let canvas = Canvas(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 400, height: 300)))
            canvas.backcolor = Color.brown.cgcolor
            
            let canvastwo = Canvas(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 400, height: 300)))
            canvastwo.backcolor = Color.darkbrown.cgcolor
            
            var model = Fridge(screen: [Float(400), Float(300)])

            
            canvas.setDrawables([model])
            
            
            canvas.mousedrug = { point in
                model.set(point: point)
                canvas.setDrawables([model])
            }
            
            
            defer {
                DispatchQueue.global().async {
                    var command = ""
                    while command != "end" {
                        print("input command:")
                        guard let line = readLine() else { continue }
                        switch line {
                        case "power",  "p": model.input = .power
                        case "rotate", "r": model.input = .direction
                        case "move",   "m": model.input = .move
                        case "length", "l": model.input = .length
                        case "height", "h": model.input = .height
                        case "toolsleem", "ts": model.tool = .slim
                        case "toolgrowth", "tg": model.tool = .growth
                        case "toolwaist", "tw": model.tool = .waist
                        default: break
                        }
                        command = line
                    }
                    print("end commands")
                }
            }
            
            return ([canvas], [], [], [1])
        }
        
        
        /// Вызывается в методе вью контроллера
        func system(a: Int) {}
        
        
        let packet = ControllerPacket(initialClosure: initialdraw, systemCallClosure: system)
        return packet
    }
}
#endif
