struct DoteMover {
    func move(dotes: [SIMD2<Float>], slim: Slim, power: Float, sign: Float, tool: Fridge.Tool) -> [SIMD2<Float>] {
        return dotes.map { dote in 
            var dote = dote
            let delta: SIMD2<Float>
            switch tool {
            case .growth: 
                delta = GrowerPerDote(point: dote, slim: slim, sign: sign, power: power).delta
            case .slim: 
                delta = SlimmerPerDote(point: dote, slim: slim, sign: sign, power: power).delta
            case .waist:
                delta = WaisterPerDote(point: dote, brush: slim, sign: sign, power: power).delta
            case .smooth:
                delta = SmootherPerDote(point: dote, brush: slim, sign: sign, power: power).delta
            }
            dote += delta
            return dote
        }
    }
}
