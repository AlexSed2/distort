import Draw
import simd
import Textures

public protocol SquareAxisBrush {
    var center: SIMD2<Float> { get }
    var direction: SIMD2<Float> { get }
    var oLongest: Float { get }
    var oHiegher: Float { get }
    var color: Color { get }
    var strokeWidth: Double { get }
}



public extension SquareAxisBrush {
    
    var color: Color { Color.fieldMarkup }
    
    var triangel: (ab: SIMD3<Float>, h: SIMD3<Float>) {
        ([direction.x, direction.y, 0], [0, 0, 1])
    }
    
    var crossvector: SIMD2<Float> {
        let z = simd_normalize(simd_cross(triangel.ab, triangel.h))
        return [z.x, z.y]
    }
    
    var axis: (SIMD2<Float>, SIMD2<Float>) {
        let upvector = direction * oLongest + center
        let downvector = -direction * oLongest + center
        return (upvector, downvector)
    }
    var front: (SIMD2<Float>, SIMD2<Float>) {
        let c = crossvector * oHiegher
        return (axis.0 + c, axis.1 + c)
    }
    var back: (SIMD2<Float>, SIMD2<Float>) {
        let c = crossvector * oHiegher
        return (axis.0 - c, axis.1 - c)
    }
    var left: (SIMD2<Float>, SIMD2<Float>) {
        (front.0, back.0)
    }
    var right: (SIMD2<Float>, SIMD2<Float>) {
        (front.1, back.1)
    }
    
    func draw(into renderer: Renderer) {
        let cross = Cross(center: center, diagonal: [20, 20], tail: 10)
        cross.draw(into: renderer)
        
        renderer.setColor(color: color)
        renderer.setWidth(w: strokeWidth)
        renderer.drawPath(points: [axis.0.cgpoint, axis.1.cgpoint])
        renderer.drawPath(points: [front.0.cgpoint, front.1.cgpoint])
        renderer.drawPath(points: [back.0.cgpoint, back.1.cgpoint])
        renderer.drawPath(points: [left.0.cgpoint, left.1.cgpoint])
        renderer.drawPath(points: [right.0.cgpoint, right.1.cgpoint])
    }
    
}
