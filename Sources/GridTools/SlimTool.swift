import Draw
import simd

public struct Slim: SquareAxisBrush, Drawable, Comparable {
    
        
    public var center: SIMD2<Float>
    public var direction: SIMD2<Float>
    public var oLongest: Float
    public var oHiegher: Float
    public var color: Color
    public var strokeWidth: Double
    
    
    public init(center: SIMD2<Float>, direction: SIMD2<Float>, oLongest: Float, oHiegher: Float, color: Color) {
        self.center = center
        self.direction = direction
        self.oLongest = oLongest
        self.oHiegher = oHiegher
        self.color = color
        self.strokeWidth = 1.0
    }
    
    public mutating func setWheel(coordinatesOnScreen: SIMD2<Float>) {
        let diff = center - coordinatesOnScreen
        let norm = simd_normalize(diff)
        direction = norm
    }
    
    public mutating func set(center: SIMD2<Float>) {
        self.center = center
    }
    
    public var angel: Float {
        let normalized = direction
        guard normalized.x != 0 else { return 0 }
        let tana = normalized.y / normalized.x
        var a = atan(tana)
        if normalized.x < 0 && normalized.y > 0 { a += Float.pi }
        if normalized.x < 0 && normalized.y < 0 { a += Float.pi }
        return a
    }
    
    public static func < (lhs: Slim, rhs: Slim) -> Bool {
        lhs.angel < rhs.angel
    }
    
}

public struct FindAngel {
    var preview: Slim?
    public mutating func touchUp() { preview = nil }
    public init() {}
    public mutating func getAngel(slim: Slim) -> Float {
        guard let prev = preview else {
            preview = slim
            return 0
        }
        let first = prev.direction
        let second = slim.direction
        let uf = simd_normalize(first)
        let us = simd_normalize(second)
        let prod = simd_dot(uf, us)
        var a = acos(prod)
        if prev < slim { a = -a }
        preview = slim
        return a
    }
}
