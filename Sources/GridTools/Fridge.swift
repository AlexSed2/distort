import Draw
import Grid
import simd
import CoreGraphics

struct Fridge: Drawable {
    
    enum Tool { case slim, growth, waist, smooth }
    enum InputMode { case move, direction, power, length, height }
    
    var dots: [SIMD2<Float>]
    let matricies: MatrixTransforms
    var brush: Slim
    var tool: Tool
    var input: InputMode
    
    var slimerAt50: SlimmerPerDote?
    
    init(screen: SIMD2<Float>) {
        let grid = PlaneGrid(diagonal: [390, 290], parts: 12)
        let slots = grid.getAllSlots()
        matricies = MatrixTransforms(screen: screen)
        dots = []
        brush = Slim(center: [200, 150], direction: [1, 0], oLongest: 100, oHiegher: 100, color: Color.fieldMarkup)
        input = .power
        tool = .smooth
        let z = slots.map { slot -> SIMD2<Float> in
            let position3: SIMD3<Float> = [slot.positionf.x, slot.positionf.y, 1]
            let transformed3 = position3 * matricies.translateToCenter
            let transformed2 = SIMD2<Float>(transformed3.x, transformed3.y)
            return transformed2
        }
        dots = z
    }
    
    mutating func set(point: CGPoint) {
        let vector: SIMD2<Float> = [Float(point.x), Float(point.y)]
        print("vector: \(vector)")
        switch input {
        case .move: brush.set(center: vector)
        case .direction: brush.setWheel(coordinatesOnScreen: vector)
        case .length: brush.oLongest = vector.x
        case .height: brush.oHiegher = vector.y
        case .power: power(point: vector)
        }
        setSlimer()
    }
    
    mutating func setSlimer() {
        slimerAt50 = SlimmerPerDote(point: dots[50], slim: brush, sign: 1, power: 1)
    }
    
    var preview: SIMD2<Float>?
    mutating func power(point: SIMD2<Float>) {
        let current = point
        if let p = preview {
            let delta = p - current
            var sign: Float = 1
            if current.y < p.y { sign = -1 }
            let deltavector = simd_length(delta) / 10
            if tool == .growth {
                brush.oHiegher += deltavector * sign
            }
            print("sign: \(sign), deltavector: \(deltavector)")
            dots = DoteMover().move(dotes: dots, slim: brush, power: deltavector, sign: sign, tool: tool)
        }
        preview = current
    }
    
    func draw(into renderer: Renderer) {
        dots.forEach {
            renderer.circleAt(point: $0.cgpoint, radius: 2)
            renderer.setColor(color: Color.backbrownrender)
            renderer.stroke()
        }
        brush.draw(into: renderer)
        slimerAt50?.draw(into: renderer)
    }
    
    
}

struct CheckMultyloyOrder {
    
    func check() {
        
        let delta: SIMD2<Float> = [100, 100]
        
        var matrix3x3 = matrix_identity_float3x3
        matrix3x3[0, 2] = delta.x
        matrix3x3[1, 2] = delta.y
        
        var matrix4x4 = matrix_identity_float4x4
        matrix4x4[3, 0] = delta.x
        matrix4x4[3, 1] = delta.y
        
        let dote3: SIMD3<Float> = [10, 10, 1]
        let dote4: SIMD4<Float> = [10, 10, 0, 1]
        
        let r1 = matrix3x3 * dote3 // SIMD3<Float>(10.0, 10.0, 2001.0)
        let r2 = dote3 * matrix3x3 // SIMD3<Float>(110.0, 110.0, 1.0)
        let r3 = matrix4x4 * dote4 // SIMD4<Float>(110.0, 110.0, 0.0, 1.0)
        let r4 = dote4 * matrix4x4 // SIMD4<Float>(10.0, 10.0, 0.0, 2001.0)
        
        print(r1)
        print(r2) 
        print(r3)
        print(r4)
        
    }
    
}
