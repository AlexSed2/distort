import simd

protocol AxisMoover {
    associatedtype Brush: SquareAxisBrush 
    var point: SIMD2<Float> { get set }
    var brush: Brush { get }
    var sign: Float { get }
    var power: Float { get }
    var weight: Float { get } // Customisation point
}

extension AxisMoover {
    
    var triangel: (ab: SIMD3<Float>, h: SIMD3<Float>) {
        var h: Float = 1
        
        let a = brush.center
        let b = brush.center + brush.direction
        let p = point
        
        let d = (p.x - a.x) * (b.y - a.y) - (p.y - a.y) * (b.x - a.x)
        if d < 0 { h *= -1 }
        if sign < 0 { h *= -1 }
        
        return ([brush.direction.x, brush.direction.y, 0], [0, 0, h])
    }
    
    var crossvector: SIMD2<Float> {
        let z = simd_normalize(simd_cross(triangel.ab, triangel.h))
        return [z.x, z.y]
    }
    
    
    var projection: Float {
        let b = point - brush.center
        let scalar = simd_dot(brush.direction, b)
        let length = scalar / simd_length(brush.direction)
        return length
    }
    
    var projectionVector: SIMD2<Float> {
        brush.direction * projection
    }
    
    var powerVector: SIMD2<Float> {
        let d = point - brush.center
        let c = projectionVector
        let p = d - c
        return -p
    }
    
    // чем больше вес тем дальше едет вертекс
    var weight: Float {
        
        
        let weight = simd_length(powerVector)
        
        // Противодействие
        var знаменатель = brush.oHiegher
        var числитель = brush.oHiegher - weight 
        let heightFactor = числитель / знаменатель
        var сила = weight * heightFactor
        
        // соотношение сторон после котрой начинает действовать сила краев оси
        let portion: Float = 0.5 
        
        знаменатель = brush.oLongest * portion
        let h = simd_length(projectionVector)
        let diff = brush.oLongest - h 
        числитель = diff
        
        
        if diff < brush.oLongest * portion {
            сила *= sqrt(числитель / знаменатель)
        }
        
        if weight < brush.oHiegher && h < brush.oLongest { return сила }
        else { return 0 }
        
    }
    
    var delta: SIMD2<Float> {
        return crossvector * weight * power
    }
}
