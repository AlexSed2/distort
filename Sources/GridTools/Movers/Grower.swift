import simd
import Math
import Draw
import CoreGraphics

struct GrowerPerDote: Drawable {    
    
    var point: SIMD2<Float>
    var slim: Slim
    var sign: Float
    var power: Float
    
    var triangel: (ab: SIMD3<Float>, h: SIMD3<Float>) {
        var h: Float = 1
        
        let a = slim.center
        let b = slim.center + slim.direction
        let p = point
        
        let d = (p.x - a.x) * (b.y - a.y) - (p.y - a.y) * (b.x - a.x)
        if d < 0 { h *= -1 }
        if sign < 0 { h *= -1 }
        
        return ([slim.direction.x, slim.direction.y, 0], [0, 0, h])
    }
    
    var crossvector: SIMD2<Float> {
        let z = simd_normalize(simd_cross(triangel.ab, triangel.h))
        return [z.x, z.y]
    }
    
    
    var projection: Float {
        let b = point - slim.center
        let scalar = simd_dot(slim.direction, b)
        let length = scalar / simd_length(slim.direction)
        return length
    }
    
    var projectionVector: SIMD2<Float> {
        slim.direction * projection
    }
    
    var powerVector: SIMD2<Float> {
        let d = point - slim.center
        let c = projectionVector
        let p = d - c
        return -p
    }
    
    
    // чем больше вес тем дальше едет вертекс
    var weight: Float {
        let weight = simd_length(powerVector)
        if weight < slim.oHiegher  { return weight / slim.oHiegher  }
        else { return 1 }
        
    }
    
    
    var delta: SIMD2<Float> {
        crossvector * power * weight
    }
    
    // Utils
    
    
    var drawnormal: SIMD2<Float> {
        let z = crossvector * 20 + point
        return z
    }
    
    func draw(into renderer: Renderer) {
        renderer.setColor(color: Color.orange)
        renderer.drawPath(points: [point.cgpoint, drawnormal.cgpoint])
        renderer.setColor(color: Color.red)
        let d = powerVector + point
        renderer.drawPath(points: [point.cgpoint, d.cgpoint])
    }
    
}
