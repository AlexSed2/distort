import simd

struct SmootherPerDote: AxisMoover {    
    var point: SIMD2<Float>
    var brush: Slim
    var sign: Float
    var power: Float
    
    
    // чем больше вес тем дальше едет вертекс
    var weight: Float {
        
        let weight = simd_length(powerVector)
        
        // https://en.wikipedia.org/wiki/Window_function#Hann_and_Hamming_windows
        
        let N = brush.oLongest * 2
        let n = simd_length(projectionVector) - 1 - brush.oLongest
        let s = sin(Float.pi * n / N)
        let w = s * s

        let f = Math().getN(a: brush.oHiegher, b: simd_length(powerVector))
        
        let nuller: Float
        if simd_length(projectionVector) > brush.oLongest { nuller = 0 }
        else if simd_length(powerVector) > brush.oHiegher {nuller = 0}
        else { nuller = 1 }
        
        return  weight * (f / 10) * w * nuller
        
    }
    
}

