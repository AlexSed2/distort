import simd

struct WaisterPerDote: AxisMoover {    
    var point: SIMD2<Float>
    var brush: Slim
    var sign: Float
    var power: Float

    
    // чем больше вес тем дальше едет вертекс
    var weight: Float {
        
        
        var weight = simd_length(powerVector)
        let w = weight - brush.oHiegher / 3 + weight / 10 
        weight = w <= 0 ? weight / 10 : w + weight / 10
        
//        return weight
        
        // Противодействие по OY
        var знаменатель = brush.oHiegher
        var числитель = brush.oHiegher - simd_length(powerVector) 
        let heightFactor = числитель / знаменатель
        var сила = weight * heightFactor
        
        // Противодействие от OO по OX для скругления талии
        let r = brush.oLongest
        let x = projection
        var y = sqrt(r * r - x * x)
        y = abs(y * y * y) // можно убрать куб
        сила *= y 
        
        
        сила /= 1000000
        
        // соотношение сторон после котрой начинает действовать сила краев оси
        let portion: Float = 1
        знаменатель = brush.oLongest * portion
        let h = simd_length(projectionVector)
        let diff = brush.oLongest - h 
        числитель = diff
        
        
        
        if simd_length(powerVector) < brush.oHiegher 
        && h < brush.oLongest { return сила }
        else { return 0 }
    }
    
}
