import simd
import Math

struct MatrixTransforms {
    let screen: SIMD2<Float>
    var translateToCenter: simd_float3x3 {
        Matricies().translate(by: screen / 2)
    } 
}
