import Draw
import CoreGraphics
import simd

struct Triangel: Drawable {    
    var A: SIMD2<Double>
    var B: SIMD2<Double>
    var C: SIMD2<Double>
    
    func draw(into renderer: Renderer) {
        renderer.move(to: A.cgpoint)
        renderer.drawPath(points: [A.cgpoint, B.cgpoint])
        renderer.drawPath(points: [B.cgpoint, C.cgpoint])
        renderer.drawPath(points: [C.cgpoint, A.cgpoint])
    }
    
}

struct CosXEqXFridge: Drawable {

    var input = Fridge.InputMode.length
    var tool = Fridge.Tool.growth
    var triangel: Triangel
    let screen: SIMD2<Double>
    let matricies: MatrixTransforms
    
    init(screen: SIMD2<Double>) {
        
        self.screen = screen
        matricies = MatrixTransforms(screen: SIMD2<Float>(screen))
        
        let A: SIMD2<Double> = [-100, -100]
        let B: SIMD2<Double> = [100, -100]
        let C: SIMD2<Double> = [100, 100]
        
        
        triangel = Triangel(A: A, B: B, C: C)
    }
    
    mutating func set(point: CGPoint) {
        let vector: SIMD2<Float> = [Float(point.x), Float(point.y)]
        let position3: SIMD3<Float> = [Float(vector.x), Float(vector.y), 1]
        let transformed3 = position3 * matricies.translateToCenter.inverse
        let transformed2 = SIMD2<Float>(transformed3.x, transformed3.y)
        switch input {
        case .length: 
            triangel.C.x = SIMD2(transformed2).x
            triangel.B.x = SIMD2(transformed2).x
        case .height: 
            triangel.C.y = SIMD2(transformed2).y
        default: break
        }
        checkCos()
    }
    
    func checkCos() {
        let c = simd_length(triangel.B - triangel.A)
        let b = simd_length(triangel.C - triangel.A)
        let cosinus = c / b
        let angel = acos(cosinus)
        print("cosinus: \(cosinus), angel: \(angel)")
    }
    
    func map(vector: inout SIMD2<Double>) {
        let position3: SIMD3<Float> = [Float(vector.x), Float(vector.y), 1]
        let transformed3 = position3 * matricies.translateToCenter
        let transformed2 = SIMD2<Float>(transformed3.x, transformed3.y)
        vector = SIMD2<Double>(transformed2)
    }
    
    func draw(into renderer: Renderer) {
        renderer.setColor(color: Color.backbrownrender)
        renderer.setWidth(w: 1.0)
        
        var t = triangel
        map(vector: &t.A)
        map(vector: &t.B)
        map(vector: &t.C)
        
        t.draw(into: renderer)
        renderer.stroke()
    }
}
