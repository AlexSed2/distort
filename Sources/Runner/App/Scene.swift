import App
import DistortPhoto
import GridTools

struct Apps {
    var distortePhoto: ControllerPacket { DistortPhoto.Setup().prepareDraw() }
    var gridTools: ControllerPacket { GridTools.Setup().prepareDraw() }
}

struct Scene {
    func run() {
        runApp()
    }
    func runApp() {
//        App(packet: Apps().gridTools).run()
        App(packet: Apps().distortePhoto).run()   
    }
    func testFridge() {
    }
    
}
