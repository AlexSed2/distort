import CoreGraphics
import CoreImage

#if canImport(UIKit)
import UIKit
#endif

public struct ImageBufferMaker {
    
    let diagonal: SIMD2<Int>

    public init(diagonal: SIMD2<Int>) {
        self.diagonal = diagonal
    }
    
    func makeBitmapContext() -> CGContext? {
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil,
                                width: diagonal.x,
                                height: diagonal.y,
                                bitsPerComponent: 8,
                                bytesPerRow: 4 * diagonal.x,
                                space: CGColorSpaceCreateDeviceRGB(),
                                bitmapInfo: bitmapInfo.rawValue)
        return context
    }
    
    public func makeImage() -> CGImage? {
        
        let white, black: CGColor
        
 
        #if os(macOS)
        white = CGColor.white
        black = CGColor.black
        #elseif os(iOS)
        white = UIColor.white.cgColor
        black = UIColor.black.cgColor
        #endif
        
        let context = makeBitmapContext()!
        context.setFillColor(white) // texture format color in Generig RGB and sRGB on model
        context.fill(CGRect(x: 0, y: 0, width: diagonal.x, height: diagonal.y))
        context.setFillColor(black)
        let center = diagonal / 2
        context.arc(center: CGPoint(x: center.x, y: center.y), from: 0, to: Double.pi, radius: Double(center.x))
        let image = context.makeImage()
        return image
    }
    
    public func makeBuffer() -> [UInt8] {
        let context = CIContext()
        var option = [CIImageOption : Any]()
        option[.colorSpace] = CGColorSpaceCreateDeviceRGB()
        var ciimage = CIImage(cgImage: makeImage()!, options: option)
        ciimage = transformScale(image: ciimage, factor: 0.9)
        let dimension = ciimage.extent.width * ciimage.extent.height
        var bitmap = [UInt8](repeating: 0, count: 4 * Int(dimension)) // 4 'инта по 8 бит' на пиксель
        context.render(ciimage,
                       toBitmap: &bitmap,
                       rowBytes: 4 * Int(ciimage.extent.width),
                       bounds: ciimage.extent,
                       format: CIFormat.RGBA8,
                       colorSpace: CGColorSpaceCreateDeviceRGB()
        )
        return bitmap
    }
    
    public func imageProcessing(image: CGImage) -> CGImage {
        let context = CIContext()
        var option = [CIImageOption : Any]()
        option[.colorSpace] = CGColorSpaceCreateDeviceRGB()
        var ciimage = CIImage(cgImage: makeImage()!, options: option)
        ciimage = transformScale(image: ciimage, factor: 0.9)
        return context.createCGImage(ciimage, from: ciimage.extent)!
    }
    
    // transforms, filters
    
    func transformScaleAndRotate(image: CIImage) -> CIImage {
        let transformScale = CGAffineTransform(scaleX: -1, y: 1)
        let transformRotate = CGAffineTransform(rotationAngle: CGFloat(exactly: Double.pi / 2)!)
        let transformed = image.transformed(by: transformScale).transformed(by: transformRotate)
        return transformed
    }
    
    func transformScale(image: CIImage, factor: CGFloat) -> CIImage {
        let transformScale = CGAffineTransform(scaleX: factor, y: factor)
        let transformed = image.transformed(by: transformScale)
        return transformed
    }
    
    // debug draw
    func draw(to context: CGContext) {
        let rect = CGRect(x: 0, y: 0, width: diagonal.x, height: diagonal.y)
        var image = makeImage()!
        image = imageProcessing(image: image)
        context.draw(image, in: rect, byTiling: false)
    }
}
