import Draw
import CoreGraphics

public struct CanvasTexture: Drawable {
    let diagonal: SIMD2<Int>
    let image: CGImage
    public func draw(into renderer: Renderer) {
        let rect = CGRect(x: 0, y: 0, width: diagonal.x, height: diagonal.y)
        renderer.draw(image: image, in: rect)
    }
}
