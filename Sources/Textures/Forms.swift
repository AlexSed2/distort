import Draw
import CoreGraphics
import Math

#if canImport(Appkit)
import AppKit
#elseif canImport(UIKit)
import UIKit
#endif



protocol Form {
    func draw(into context: CGContext)
}


public struct Body {
    let position: SIMD2<Double>
    let diagonal: SIMD2<Double>
    public init(position: SIMD2<Double>, diagonal: SIMD2<Double>) {
        self.position = position
        self.diagonal = diagonal
    }
    public init(position: SIMD2<Int>, diagonal: SIMD2<Int>) {
        self.position = SIMD2<Double>(position)
        self.diagonal = SIMD2<Double>(diagonal)
    }
}

struct Numbers {
    
}

struct Number: Form {
    let body: Body
    let number: Int
    
    func draw(into context: CGContext) {
        let start: SIMD2<Double> = body.diagonal / 7
        let fontsize = Int(body.diagonal.y * 0.86)
        let string = number < 10 ? "0\(number)" : "\(number)"
        
        let color: Color
        
        #if os(macOS)
        color = Color.bodyrender
        #elseif os(iOS)
        color = Color.body
        #endif
        
        draw(text: string, 
             at: CGPoint(x: start.x, y: start.y), 
             fontsize: fontsize, 
             color: color, 
             backcolor: color, 
             context: context,
             firstempty: number < 10)
    }
    
    
    func draw(text: String, at point: CGPoint, fontsize: Int, color: Color, backcolor: Color, context: CGContext, firstempty: Bool) {
        
        let testcolor: CGColor
        #if os(macOS)
        let h1 = NSFont.init(name: "Digital Dismay", size: CGFloat(fontsize)) ?? NSFont.systemFont(ofSize: CGFloat(fontsize))
        testcolor = CGColor.clear
        #elseif os(iOS)
        var h1 = UIFont.init(name: "Digital Dismay", size: CGFloat(fontsize)) ?? UIFont.systemFont(ofSize: 8)
        testcolor = UIColor.clear.cgColor
        #endif
        
        let lineText = NSMutableAttributedString(string: text)
        let lineTextDark = NSMutableAttributedString(string: "88")
        
        let numbercolor = Color.greenneonfull.cgcolor
        let color = color.cgcolor
        let backcolor = Color.sameJustDarken(color: backcolor, k: 0.95).cgcolor
        
        
        lineText.addAttributes([NSAttributedString.Key.font : h1,
                                NSAttributedString.Key.foregroundColor: numbercolor],
                               range: NSMakeRange(0,lineText.length))
        
        if firstempty {
            lineText.addAttributes([NSAttributedString.Key.font : h1,
                                    NSAttributedString.Key.foregroundColor: testcolor],
                                   range: NSMakeRange(0, 1))
        }
        
        
        lineTextDark.addAttributes([NSAttributedString.Key.font : h1,
                                    NSAttributedString.Key.foregroundColor: backcolor],
                                   range: NSMakeRange(0,lineTextDark.length))
        
        
        let lineToDraw: CTLine = CTLineCreateWithAttributedString(lineText)
        let lineToDrawDark: CTLine = CTLineCreateWithAttributedString(lineTextDark)
        context.setTextDrawingMode(.fill)
        context.textPosition = point
        context.setFillColor(color) // texture format color in Generig RGB and sRGB on model
        context.fill(CGRect(x: 0, y: 0, width: body.diagonal.x, height: body.diagonal.y))
        CTLineDraw(lineToDrawDark, context)
        context.textPosition = point
        CTLineDraw(lineToDraw, context)
    }
    
}

struct Rect: Form {    
    let body: Body
    var origin: SIMD2<Double> { body.position - body.diagonal / 2 }
    var corners: [SIMD2<Double>] {
        var corners = [SIMD2<Double>]()
        var corner: SIMD2<Double>
        corner = body.position - body.diagonal / 2
        corners.append(corner)
        corner = body.position + body.diagonal / 2
        corners.append(corner)
        var diff = body.position - origin
        diff.y *= -1
        corner = body.position + diff
        corners.append(corner)
        corner = body.position - diff
        corners.append(corner)
        return corners
    }
    func draw(into context: CGContext) {
        let origin = CGPoint(x: self.origin.x, y: self.origin.y)
        let size = CGSize(width: body.diagonal.x, height: body.diagonal.y)
        let rect = CGRect(origin: origin, size: size)
        context.stroke(rect)
    }
    func fill(into context: CGContext, color: CGColor) {
        let origin = CGPoint(x: self.origin.x, y: self.origin.y)
        let size = CGSize(width: body.diagonal.x, height: body.diagonal.y)
        let rect = CGRect(origin: origin, size: size)
        context.setFillColor(color)
        context.fill(rect)
    }
    func draw(into renderer: Renderer, color: Color) {
        let origin = CGPoint(x: self.origin.x, y: self.origin.y)
        let size = CGSize(width: body.diagonal.x, height: body.diagonal.y)
        let rect = CGRect(origin: origin, size: size)
        renderer.draw(color: color, in: rect)
    }
}


struct Circle: Form {
    
    let body: Body
    
    func draw(into context: CGContext) {
        let white: CGColor
        #if os(macOS)
        white = CGColor.white
        #elseif os(iOS)
        white = UIColor.white.cgColor
        #endif
        context.setFillColor(white) // texture format color in Generig RGB and sRGB on model
        context.setFillColor(Color.fieldMarkup.cgcolor)
        let center = body.position
        context.addArc(center: CGPoint(x: center.x, y: center.y), 
                       radius: CGFloat(Double(body.diagonal.x)), 
                       startAngle: CGFloat(0), 
                       endAngle: CGFloat(Double.pi * 2), 
                       clockwise: false)
        context.fillPath(using: CGPathFillRule.winding)
    }
    
}

struct CrossPlane: Form {
    
    let selfdiagonal: SIMD2<Int>
    let fieldsquarediagonal: SIMD2<Int>
    let gatelength: Double
    let barrelradius: Double
    let side: SurfaceKey
    
    func draw(into context: CGContext) {
        
        let clear: CGColor
        #if os(macOS)
        clear = CGColor.clear
        #elseif os(iOS)
        clear = UIColor.clear.cgColor
        #endif
        
        context.setFillColor(clear)
        context.fill(CGRect(x: 0, y: 0, width: selfdiagonal.x, height: selfdiagonal.y))
        context.setStrokeColor(Color.fieldMarkup.cgcolor)
        
        context.setWidth(w: 2.0)
        let geometry = CrossPlaneGeometry(texturediagonal: selfdiagonal, fielddiagonal: fieldsquarediagonal, gatelength: gatelength)
        geometry.bigrect.draw(into: context)
        
        let corners = geometry.barrels
        let radius = barrelradius * 2.0
        corners.forEach { corner in
            let cross = Circle(body: Body(position: corner, diagonal: [radius, radius]))
            cross.draw(into: context)
        }
        
        context.setWidth(w: 1.0)

        let begin = CGPoint(x: selfdiagonal.x / 2, y: 0)
        let end = CGPoint(x: selfdiagonal.x / 2, y: selfdiagonal.y)
        context.move(to: begin)
        context.strokeLineSegments(between: [begin, end])
        
        let center = geometry.bigrect.body.position
        
        let circle = Circle(body: Body(position: center, diagonal: [5, 5]))
        circle.draw(into: context)
        
        let offset: SIMD2<Double> = [Double(selfdiagonal.x) / 6, 0]
        switch side {
        case .leftmoving: 
            addLeftTriangel(into: context, offset: offset, geometry: geometry)
        case .rightmoving: 
            addRightTriangel(into: context, offset: offset, geometry: geometry)
        case .all:
            addLeftTriangel(into: context, offset: offset, geometry: geometry)
            addRightTriangel(into: context, offset: offset, geometry: geometry)
        case .neutral: break
        }
    
    }
    
    func addLeftTriangel(into context: CGContext, offset: SIMD2<Double>, geometry: CrossPlaneGeometry) {
        let one   = (geometry.barrels[0] + offset).cgpoint
        let two   = (geometry.barrels[1] + offset).cgpoint
        let three = (SIMD2<Double>(selfdiagonal)/2 - offset).cgpoint
        
        context.move(to: one)
        context.line(to: two)
        context.line(to: three)
        context.line(to: one)
        
        context.setStrokeColor(Color.greenneonfull.cgcolor)
        context.setLineCap(.round)
        context.setLineWidth(2.0)
        context.stroke()
    }
    func addRightTriangel(into context: CGContext, offset: SIMD2<Double>, geometry: CrossPlaneGeometry) {
        
        let four = (geometry.barrels[2] - offset).cgpoint
        let five = (geometry.barrels[3] - offset).cgpoint
        let six = (SIMD2<Double>(selfdiagonal) / 2 + offset).cgpoint
        
        context.move(to: six)
        context.line(to: four)
        context.line(to: five)
        context.line(to: six)
        
        context.setStrokeColor(Color.greenneonfull.cgcolor)
        context.setLineCap(.round)
        context.setLineWidth(2.0)
        context.stroke()
    }
}

struct CircleOnPlane: Form {
    let circle: Circle
    let rect: Rect
    init(texturediagonal: SIMD2<Int>, circleposition: SIMD2<Int>, circlediagonal: SIMD2<Int>) {
        rect = Rect(body: Body(position: texturediagonal / 2, diagonal: texturediagonal))
        circle = Circle(body: Body(position: circleposition, diagonal: circlediagonal))
    }
    func draw(into context: CGContext) {
        
        context.setFillColor(Color.backbrown.cgcolor)
        context.setStrokeColor(Color.greenneon.cgcolor)
        context.setLineWidth(1.0)
        
        rect.draw(into: context)
        circle.draw(into: context)
        
    }
}

public struct Cross: Drawable {    
    let body: Body
    let tail: Double
    public init(center: SIMD2<Float>, diagonal: SIMD2<Float>, tail: Float) {
        let body = Body(position: SIMD2<Double>(center), diagonal: SIMD2<Double>(diagonal))
        self.init(body: body, tail: Double(tail))
    }
    init(body: Body, tail: Double) {
        self.body = body
        self.tail = tail
    }
    var rects: [Rect] {
        let vdiagonal = SIMD2<Double>([body.diagonal.x, tail])
        let vertical = Rect(body: Body(position: body.position, diagonal: vdiagonal))
        let hdiagonal = SIMD2<Double>([tail, body.diagonal.y])
        let horisontal = Rect(body: Body(position: body.position, diagonal: hdiagonal))
        return [vertical, horisontal]
    }
    public func draw(into context: CGContext) {
        rects.forEach { $0.fill(into: context, color: Color.fieldMarkup.cgcolor) }
    }
    public func draw(into renderer: Renderer) {
        rects.forEach { $0.draw(into: renderer, color: Color.fieldMarkup) }
    }
}

struct BarrelCross {
    let body: Body
    let tail: Double
    var rects: [Rect] {
        let vdiagonal = SIMD2<Double>([body.diagonal.x, tail * 0.7])
        let vertical = Rect(body: Body(position: body.position, diagonal: vdiagonal))
        let hdiagonal = SIMD2<Double>([tail, body.diagonal.y])
        let horisontal = Rect(body: Body(position: body.position, diagonal: hdiagonal))
        return [vertical, horisontal]
    }
    func draw(into context: CGContext) {
        rects.forEach { $0.fill(into: context, color: Color.fieldMarkup.cgcolor) }
    }
}
