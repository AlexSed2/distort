import Draw
import CoreGraphics
import Foundation
#if canImport(Appkit)
import AppKit
#elseif canImport(UIKit)
import UIKit
#endif

import CoreImage


var context = CIContext()


public struct Textures {
    
    public enum SamplerStyle { case simple, hard }
    
    public static var textures: [SIMD2<Int>: MTLTexture] = [:]
    public static var samplers: [SamplerStyle: MTLSamplerState] = [:]
    public static var imagesNumbers200x200: [Int: [UInt8]] = [:]
    public static let cicontext = CIContext()
    public static var gradientTexture200x200: MTLTexture?
    public static var gradientBuffer200x200: [UInt8]?
    
    public static var gradientTexture200x200sky: MTLTexture?
    public static var gradientBuffer200x200sky: [UInt8]?
    
    public static var device: MTLDevice?
    
    public static func setup(device: MTLDevice) throws {
        
        self.device = device
        
        let instance = Textures()
        let sampler = try instance.createSimpleSampler(device: device)
        samplers[.simple] = sampler
        
        // Здесь создаём текстуры нужного размера
        let size: SIMD2<Int> = [200, 200]
        let texture200x200 = try instance.createTexture(device: device, size: size)
        textures[size] = texture200x200
        
        
        let buffer = instance.createNumberBuffer(size: size)
        imagesNumbers200x200[4] = buffer
        
        
        gradientTexture200x200 = try instance.createTexture(device: device, size: size)
        gradientTexture200x200sky = try instance.createTexture(device: device, size: size)
        
        gradientBuffer200x200 = instance.createGradientBuffer(size: size)
        gradientBuffer200x200sky = instance.createGradientSkyBuffer(size: size)
        
        setupFirstTexture()
        setupTexture(texture: gradientTexture200x200!, size: size, buffer: gradientBuffer200x200!)
        setupTexture(texture: gradientTexture200x200sky!, size: size, buffer: gradientBuffer200x200sky!)
    }
    
    
    func createNumberBuffer(size: SIMD2<Int>) -> [UInt8] {
        let buffer = NumberImage(size: size, number: 10).makeBuffer()!
        return buffer
    }
    
    func createGradientBuffer(size: SIMD2<Int>) -> [UInt8] {
        let buffer = NumberImage(size: size, number: 0).makeGradientBuffer()!
        return buffer
    }
    
    func createGradientSkyBuffer(size: SIMD2<Int>) -> [UInt8] {
        let buffer = NumberImage(size: size, number: 0).makeGradientSkyBuffer()!
        return buffer
    }
    
    func createSimpleSampler(device: MTLDevice) throws ->  MTLSamplerState {
        let samplerDescriptor = MTLSamplerDescriptor()
        samplerDescriptor.sAddressMode = .clampToEdge
        samplerDescriptor.tAddressMode = .clampToEdge
        samplerDescriptor.minFilter = .nearest
        samplerDescriptor.magFilter = .linear
        samplerDescriptor.normalizedCoordinates = true
        guard let sampler = device.makeSamplerState(descriptor: samplerDescriptor) else { throw DrawError.noDescriptor }
        return sampler
    }
    
    func createTexture(device: MTLDevice, size: SIMD2<Int>) throws -> MTLTexture {
        
        let textureDescriptor = MTLTextureDescriptor.texture2DDescriptor(pixelFormat: .rgba8Unorm, width: size.x, height: size.y, mipmapped: true)
        guard let texture = device.makeTexture(descriptor: textureDescriptor) else { throw DrawError.noDescriptor }
        return texture
        
    }
    
    static func setupFirstTexture() {
        let buffer = Textures.imagesNumbers200x200[4]!
        let region = MTLRegionMake2D(0, 0, 200, 200)
        textures[[200, 200]]!.replace(region: region, mipmapLevel: 0, withBytes: buffer, bytesPerRow: 4 * 200)
    }
    
    static func setupTexture(texture: MTLTexture, size: SIMD2<Int>, buffer: [UInt8]) {
        let region = MTLRegionMake2D(0, 0, size.x, size.y)
        texture.replace(region: region, mipmapLevel: 0, withBytes: buffer, bytesPerRow: 4 * 200)
    }
    
    public enum DrawError: Error {
        case noDescriptor
        case noBuffer
    }
}

public struct NumberImage {
    
    let size: SIMD2<Int>
    let origin: SIMD2<Int>
    let number: Int
    
    public init(size: SIMD2<Int>, origin: SIMD2<Int> = .zero, number: Int) {
        self.size = size
        self.origin = origin
        self.number = number
    }
    
    public func makeBuffer() -> [UInt8]? {
        guard let image = drawNumberToBitmap() else { return nil }
        let buffer = makeBufer(image: image)
        return buffer
    }
    
    public func makeGradientBuffer() -> [UInt8]? {
        guard let image = drawGradientToBitmap() else { return nil }
        let buffer = makeBufer(image: image)
        return buffer
    }
    public func makeGradientSkyBuffer() -> [UInt8]? {
        guard let image = drawRadialGradientToBitmap() else { return nil }
        let buffer = makeBufer(image: image)
        return buffer
    }
    
    func makeBitmapContext() -> CGContext? {
        let width = size.x
        let height = size.y
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil,
                                width: width,
                                height: height,
                                bitsPerComponent: 8,
                                bytesPerRow: 4 * width,
                                space: CGColorSpaceCreateDeviceRGB(),
                                bitmapInfo: bitmapInfo.rawValue)
        return context
    }
    
    public func drawNumberToBitmap() -> CGImage? {
        guard let ctx = makeBitmapContext() else { return  nil}
        let start: SIMD2<Double> = SIMD2<Double>(size / 7)
        let fontsize = Int(Double(size.y) * 0.86)
        let image = draw(text: "\(number)", at: CGPoint(x: start.x, y: start.y), fontsize: fontsize, color: .bodyrender, backcolor: .bodyrender, context: ctx)
        return image
    }
    public func drawGradientToBitmap() -> CGImage? {
        guard let ctx = makeBitmapContext() else { return  nil}
        let image = drawGradient(context: ctx)
        return image
    }
    
    public func drawRadialGradientToBitmap() -> CGImage? {
        guard let ctx = makeBitmapContext() else { return  nil}
        let image = drawSquardGradient(context: ctx)
        return image
    }
    
    
    // Buffer
    
    func transformScaleAndRotate(image: CIImage) -> CIImage {
        let transformScale = CGAffineTransform(scaleX: -1, y: 1)
        let transformRotate = CGAffineTransform(rotationAngle: CGFloat(exactly: Double.pi / 2)!)
        let transformed = image.transformed(by: transformScale).transformed(by: transformRotate)
        return transformed
    }
    
    func transformScale(image: CIImage) -> CIImage {
        let transformScale = CGAffineTransform(scaleX: 1, y: 1)
        let transformed = image.transformed(by: transformScale)
        return transformed
    }
    
    func makeBufer(image: CGImage) -> [UInt8] {
        
        var option = [CIImageOption : Any]()
        option[.colorSpace] = CGColorSpaceCreateDeviceRGB()
        let ciimage = CIImage(cgImage: image, options: option)
        
        //        let ciimage = CIImage(cgImage: image)
        //        ciimage = transformScale(image: ciimage)
        
        let dimension = ciimage.extent.width * ciimage.extent.height
        var bitmap = [UInt8](repeating: 0, count: 4 * Int(dimension)) // 4 'инта по 8 бит' на пиксель
        //        print(ciimage.extent)
        //        let space = CGColorSpace(name: CGColorSpace.extendedSRGB)
        context.render(ciimage,
                       toBitmap: &bitmap,
                       rowBytes: 4 * Int(ciimage.extent.width),
                       bounds: ciimage.extent,
                       format: CIFormat.RGBA8,
                       colorSpace: CGColorSpaceCreateDeviceRGB()
        )
        //        print("")
        return bitmap
    }
    
    
    public func draw(into renderer: Renderer) {
        guard let image = drawNumberToBitmap() else { return }
        renderer.draw(image: image, in: CGRect(x: origin.x, y: origin.y, width: size.x, height: size.y))
    }
    
    public func drawGradient(into renderer: Renderer) {
        guard let image = drawGradientToBitmap() else { return }
        renderer.draw(image: image, in: CGRect(x: origin.x, y: origin.y, width: size.x, height: size.y))
    }
    
    public func drawRadialGradient(into renderer: Renderer) {
        guard let image = drawRadialGradientToBitmap() else { return }
        renderer.draw(image: image, in: CGRect(x: origin.x, y: origin.y, width: size.x, height: size.y))
    }
    
    
    // Drawing
    
    func draw(text: String, at point: CGPoint, fontsize: Int, color: Color, backcolor: Color, context: CGContext) -> CGImage? {
        
        #if os(macOS)
        let h1 = NSFont.init(name: "Digital Dismay", size: CGFloat(fontsize)) ?? NSFont.systemFont(ofSize: CGFloat(fontsize))
        #elseif os(iOS)
        var h1 = UIFont.init(name: "Digital Dismay", size: CGFloat(fontsize)) ?? UIFont.systemFont(ofSize: 8)
        #endif
        
        let lineText = NSMutableAttributedString(string: text)
        let lineTextDark = NSMutableAttributedString(string: "88")
        
        let numbercolor = Color.greenneonfull.cgcolor
        let color = color.cgcolor
        let backcolor = Color.sameJustDarken(color: backcolor, k: 0.95).cgcolor
        
        //        #if os(macOS)
        //        let color = color.cgcolor
        //        #elseif os(iOS)
        //        let color = UIColor.white.cgColor
        //        #endif
        
        lineText.addAttributes([NSAttributedString.Key.font : h1,
                                NSAttributedString.Key.foregroundColor: numbercolor],
                                range: NSMakeRange(0,lineText.length))
        
        lineTextDark.addAttributes([NSAttributedString.Key.font : h1,
                                    NSAttributedString.Key.foregroundColor: backcolor],
                                    range: NSMakeRange(0,lineTextDark.length))
        
        
        let lineToDraw: CTLine = CTLineCreateWithAttributedString(lineText)
        let lineToDrawDark: CTLine = CTLineCreateWithAttributedString(lineTextDark)
        context.setTextDrawingMode(.fill)
        context.textPosition = point
        context.setFillColor(color) // texture format color in Generig RGB and sRGB on model
        context.fill(CGRect(x: 0, y: 0, width: size.x, height: size.y))
        CTLineDraw(lineToDrawDark, context)
        context.textPosition = point
        CTLineDraw(lineToDraw, context)
        
//        context.setFillColor(Color.bodycian.cgcolor)
//        context.fill(CGRect(x: 0, y: 0, width: 50, height: 50))
//        context.setFillColor(Color.red.cgcolor)
//        context.fill(CGRect(x: 50, y: 50, width: 50, height: 50))
       
        let image = context.makeImage()
        return image
    }
    
//    func drawGradient(context: CGContext) -> CGImage? {
//        
//        let colors = [Color.backbrownrender.cgcolor, 
//                      Color.backbrownrender.cgcolor, 
//                      Color.horisontbrownsRGB.cgcolor, 
//                      Color.backbrownrender.cgcolor, 
//                      Color.backbrownrender.cgcolor]
//        let colorLocations: [CGFloat] = [0.0, 0.1, 0.5, 0.9, 1.0]
//        let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: colors as CFArray, locations: colorLocations)!
//        let startPoint = CGPoint.zero
//        let endPoint = CGPoint(x: 0, y: size.y)
//        context.drawLinearGradient(gradient, start: startPoint, end: endPoint, options: [])
//        let image = context.makeImage()
//        return image
//    }
    
    func drawGradient(context: CGContext) -> CGImage? {
        
        let colors = [Color.backbrownrender.cgcolor, 
                      Color.backbrownrender.cgcolor, 
                      Color.horisontbrownsRGB.cgcolor,
                      Color.horisontpinkRGB.cgcolor,
                      Color.horisontbrownsRGB.cgcolor, 
                      Color.backbrownrender.cgcolor, 
                      Color.backbrownrender.cgcolor]
        let a: CGFloat = 0.03
        let colorLocations: [CGFloat] = [0.0 + a, 0.1 + a, 0.35 + 0.05 - 0.1 + a, 0.4 + 0.05 + a, 0.52 + a, 0.9, 1.0]
        let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: colors as CFArray, locations: colorLocations)!
        let startPoint = CGPoint.zero
        let endPoint = CGPoint(x: 0, y: size.y)
        context.drawLinearGradient(gradient, start: startPoint, end: endPoint, options: [])
        let image = context.makeImage()
        return image
    }
    
    func drawSquardGradient(context: CGContext) -> CGImage? {
        
        let colors = [Color.deepskysRGB.cgcolor, Color.backbrownrender.cgcolor]
        let colorLocations: [CGFloat] = [0.0, 0.6]
        let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: colors as CFArray, locations: colorLocations)!
        let startPoint = CGPoint.zero
        let endPoint = CGPoint(x: 0, y: size.y)
        let center = size / 2 
        context.drawLinearGradient(gradient, start: startPoint, end: endPoint, options: [])
        context.drawRadialGradient(gradient, 
                                   startCenter: CGPoint(x: center.x, y: center.y), 
                                   startRadius: 0, 
                                   endCenter: CGPoint(x: center.x, y: center.y), 
                                   endRadius: CGFloat(size.x) * 0.8, options: [])
        let image = context.makeImage()
        return image
    }
    
}
