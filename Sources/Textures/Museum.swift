import Metal

public enum SurfaceKey: CaseIterable {
    case leftmoving, rightmoving, neutral, all
}

public struct Museum {
    static var device: MTLDevice?
    public static var numberDiagonal: SIMD2<Int> = [200, 200]
    public static let maximumnumber = 100
    public static var numbers: [Int: [UInt8]] = [:]
    static var surfaces: [SurfaceKey: [UInt8]] = [:]
    public static func setup(device: MTLDevice) { 
        self.device = device 
        drawAllTextures()
    }
    public static func drawAllTextures() {
        for index in 0...maximumnumber {
            let buffer = ArtStudio().draw(number: index).buffer
            Museum.numbers[index] = buffer
        }
    }
    public static func drawSurfaceBitmaps(diagonal: SIMD2<Int>, gatelength: Double, barrelraduis: Double) {
        for key in SurfaceKey.allCases {
            let buffer = ArtStudio().drawPlane(diagonal: diagonal, gatelength: gatelength, barrelradius: barrelraduis, side: key).buffer
            surfaces[key] = buffer
        }
    }
    public init() {}
    public func getEmptyTextureForNumber() -> MTLTexture {
        let textureDescriptor = MTLTextureDescriptor.texture2DDescriptor(pixelFormat: .rgba8Unorm, 
                                                                         width: Museum.numberDiagonal.x, 
                                                                         height: Museum.numberDiagonal.y, 
                                                                         mipmapped: true)
        let texture = Museum.device!.makeTexture(descriptor: textureDescriptor)!
        return texture
    }
    public func draw(number: Int, to texture: MTLTexture) {
        let region = MTLRegionMake2D(0, 0, Museum.numberDiagonal.x,Museum.numberDiagonal.y)
        let buffer = Museum.numbers[number]!
        texture.replace(region: region, mipmapLevel: 0, withBytes: buffer, bytesPerRow: 4 * Museum.numberDiagonal.x)
    }
    public func getEmptyTextureForSurface(diagonal: SIMD2<Int>) -> MTLTexture {
        let textureDescriptor = MTLTextureDescriptor.texture2DDescriptor(pixelFormat: .rgba8Unorm, 
                                                                         width: diagonal.x, 
                                                                         height: diagonal.y, 
                                                                         mipmapped: true)
        let texture = Museum.device!.makeTexture(descriptor: textureDescriptor)!
        return texture
    }
    public func drawSideToSurface(texture: MTLTexture, side: SurfaceKey, diagonal: SIMD2<Int>) {
        let buffer = Museum.surfaces[side]!
        let region = MTLRegionMake2D(0, 0, diagonal.x, diagonal.y)
        texture.replace(region: region, mipmapLevel: 0, withBytes: buffer, bytesPerRow: 4 * diagonal.x)
    }
    public func drawSurface(diagonal: SIMD2<Int>, gatelength: Double, barrelraduis: Double) -> MTLTexture {
        let buffer = ArtStudio().drawPlane(diagonal: diagonal, gatelength: gatelength, barrelradius: barrelraduis, side: .neutral).buffer
        let textureDescriptor = MTLTextureDescriptor.texture2DDescriptor(pixelFormat: .rgba8Unorm, 
                                                                         width: diagonal.x, 
                                                                         height: diagonal.y, 
                                                                         mipmapped: true)
        let texture = Museum.device!.makeTexture(descriptor: textureDescriptor)!
        let region = MTLRegionMake2D(0, 0, diagonal.x, diagonal.y)
        texture.replace(region: region, mipmapLevel: 0, withBytes: buffer, bytesPerRow: 4 * diagonal.x)
        return texture
    }
}
