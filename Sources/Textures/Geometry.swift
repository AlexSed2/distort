import CoreGraphics



public struct CrossPlaneGeometry {
    let texturediagonal: SIMD2<Double>
    let fielddiagonal: SIMD2<Double>
    let gatelength: Double
    
    public var centralbarrels: [SIMD2<Double>] {
        let offset: SIMD2<Double> = texturediagonal / 2
        return barrels.map { $0 - offset }
    }
    
    public var barrels: [SIMD2<Double>] {
        
        let vector: SIMD2<Double> = [0, gatelength / 2]
        
        let t, v: SIMD2<Double>
        let u, w: SIMD2<Double>
        
        t =  vector
        v = -vector
        u =  vector
        w = -vector
        
        let leftcenter: SIMD2<Double> =  [0, texturediagonal.y / 2]
        let rightcenter: SIMD2<Double> = [texturediagonal.x, texturediagonal.y / 2] 
        var barrels = [SIMD2<Double>]()
        barrels.append(leftcenter + t)
        barrels.append(leftcenter + v)
        barrels.append(rightcenter + u)
        barrels.append(rightcenter + w)
        
        return barrels
    }
    
    public init(texturediagonal: SIMD2<Int>, fielddiagonal: SIMD2<Int>, gatelength: Double) {
        self.texturediagonal = SIMD2<Double>(texturediagonal)
        self.fielddiagonal = SIMD2<Double>(fielddiagonal)
        self.gatelength = gatelength
    }
    
    var rects: [Rect] {
        let center = texturediagonal / 2
        let rectone = Rect(body: Body(position: center, diagonal: texturediagonal))
        let recttwo = Rect(body: Body(position: center, diagonal: fielddiagonal))
        return [rectone, recttwo]
    }
    
    var bigrect: Rect { rects[0] }
    var smallrect: Rect { rects[1] }

    
} 
