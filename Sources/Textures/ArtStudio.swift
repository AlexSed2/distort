public struct ArtStudio {
    public init() {}
    public func drawCircle() -> CanvasTexture {
        let diagonal: SIMD2<Int> = [200, 200]
        let circle = CircleOnPlane(texturediagonal: diagonal, circleposition: [50, 50], circlediagonal: [10, 10])
        let canvas = Canvas(diagonal: diagonal)
        let image = canvas.draw(form: circle)
        let texture = CanvasTexture(diagonal: diagonal, image: image)
        return texture
    }
    public func drawPlane(diagonal: SIMD2<Int>, gatelength: Double, barrelradius: Double, side: SurfaceKey) -> (drawable: CanvasTexture, buffer: [UInt8]) {
        let canvas = Canvas(diagonal: diagonal)
        let plane = CrossPlane(selfdiagonal: diagonal, 
                               fieldsquarediagonal: [80, 20], 
                               gatelength: gatelength, 
                               barrelradius: barrelradius,
                               side: side)
        let image = canvas.draw(form: plane)
        let texture = CanvasTexture(diagonal: diagonal, image: image)
        let buffer = canvas.drawBuffer(form: plane)
        return (texture, buffer)
    }
    public func draw(number: Int) -> (drawable: CanvasTexture, buffer: [UInt8]) {
        let diagonal: SIMD2<Int> = Museum.numberDiagonal
        let canvas = Canvas(diagonal: diagonal)
        let number = Number(body: Body(position: [0, 0], diagonal: diagonal), number: number)
        let image = canvas.draw(form: number)
        let texture = CanvasTexture(diagonal: diagonal, image: image)
        let buffer = canvas.drawBuffer(form: number)
        return (texture, buffer)
    }
}
