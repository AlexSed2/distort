import CoreGraphics
import CoreImage

struct Canvas {
    
    let diagonal: SIMD2<Int>
    let cicontext: CIContext
    let cgcontext: CGContext
    
    init(diagonal: SIMD2<Int>) {
        self.diagonal = diagonal
        self.cicontext = CIContext()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        self.cgcontext = CGContext(data: nil,
                                width: diagonal.x,
                                height: diagonal.y,
                                bitsPerComponent: 8,
                                bytesPerRow: 4 * diagonal.x,
                                space: CGColorSpaceCreateDeviceRGB(),
                                bitmapInfo: bitmapInfo.rawValue)!
    }
            
    
    func draw(form: Form) -> CGImage {
        form.draw(into: cgcontext)
        let image = cgcontext.makeImage()
        return image!
    }
    
    func drawBuffer(form: Form) -> [UInt8] {
        makeBuffer(image: draw(form: form))
    }
    
    public func makeBuffer(image: CGImage) -> [UInt8] {
        var option = [CIImageOption : Any]()
        option[.colorSpace] = CGColorSpaceCreateDeviceRGB()
        let ciimage = CIImage(cgImage: image, options: option)
        let dimension = ciimage.extent.width * ciimage.extent.height
        var bitmap = [UInt8](repeating: 0, count: 4 * Int(dimension)) // 4 'инта по 8 бит' на пиксель
        context.render(ciimage,
                       toBitmap: &bitmap,
                       rowBytes: 4 * Int(ciimage.extent.width),
                       bounds: ciimage.extent,
                       format: CIFormat.RGBA8,
                       colorSpace: CGColorSpaceCreateDeviceRGB()
        )
        return bitmap
    }
    
    // draw a form to a buffer
}
