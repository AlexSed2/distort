import MetalKit


public protocol Metalable {
    func draw(into encoder: MTLRenderCommandEncoder)
    func drawMiniShaders(into encoder: MTLRenderCommandEncoder)
}

public extension Metalable {
    func drawMiniShaders(into encoder: MTLRenderCommandEncoder) { 
//        print("Mini shaders draw is not emplemented") 
    }
}
