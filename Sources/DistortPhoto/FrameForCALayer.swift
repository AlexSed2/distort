import CoreImage

struct ImageLayerFrame {
    
    let original: CIImage
    let corners: Corners
    
    
    func getRect() -> (x: Float, y: Float) {
        
        let c = [corners.bottomLeft, corners.bottomRight, corners.topLeft, corners.topRight]
        let xmin = c.map { $0.x }.min()!
        let xmax = c.map { $0.x }.max()!
        let difx = Float(original.extent.width) - xmax
        let x = -max(abs(difx), abs(xmin))
   
        let ymin = c.map { $0.y }.min()!
        let ymax = c.map { $0.y }.max()!
        let dify = Float(original.extent.height) - ymax
        let y = -max(abs(dify), abs(ymin))
        
        return (x, y)
        
    }
    
}
