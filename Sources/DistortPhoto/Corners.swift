import CoreGraphics
import CoreImage
import simd
import Draw

public struct Corners: CustomStringConvertible, Drawable {

    var bottomLeft: SIMD2<Float>
    var topLeft: SIMD2<Float>
    var topRight: SIMD2<Float>
    var bottomRight: SIMD2<Float>
    public var matrix: simd_float3x3
    
    var bl: SIMD2<Float> { 
        let s3 = SimdConvertor().convertTo3(simd2: bottomLeft)
        let r3 = s3 * matrix
        return SimdConvertor().convertTo2(simd3: r3)
    }
    var tl: SIMD2<Float> { 
        let s3 = SimdConvertor().convertTo3(simd2: topLeft)
        let r3 = s3 * matrix
        return SimdConvertor().convertTo2(simd3: r3)
    }
    var tr: SIMD2<Float> { 
        let s3 = SimdConvertor().convertTo3(simd2: topRight)
        let r3 = s3 * matrix
        return SimdConvertor().convertTo2(simd3: r3)
    }
    var br: SIMD2<Float> { 
        let s3 = SimdConvertor().convertTo3(simd2: bottomRight)
        let r3 = s3 * matrix
        return SimdConvertor().convertTo2(simd3: r3)
    }
    
    init(diagonal: SIMD2<Float>) {
        bottomLeft = .zero
        topLeft = bottomLeft + [0, diagonal.y]
        topRight = topLeft + [diagonal.x, 0]
        bottomRight = bottomLeft + [diagonal.x, 0]
        matrix = matrix_identity_float3x3
    }
    
    mutating func set(corner: SIMD2<Float>, position: Fridge.Input.Corner) {
        
        let s3 = SimdConvertor().convertTo3(simd2: corner)
        let r3 = s3 * matrix.inverse
        let r2 = SimdConvertor().convertTo2(simd3: r3)
        
        switch position {
        case .bottomleft:
            let prev = bottomLeft
            bottomLeft = r2
            let a = topLeft - bottomLeft
            let b = bottomRight - bottomLeft
            let product = simd_cross(a, b)
            if product.z > 0 { bottomLeft = prev }
        case .bottomright:
            let prev = bottomRight
            bottomRight = r2
            let a = topRight - bottomRight
            let b = bottomLeft - bottomRight
            let product = simd_cross(a, b)
            if product.z < 0 { bottomRight = prev }
        case .topleft: 
            let prev = topLeft
            topLeft = r2
            let a = topRight - topLeft
            let b = bottomLeft - topRight
            let product = simd_cross(a, b)
            if product.z > 0 { topLeft = prev }
        case .topright:
            let prev = topRight
            topRight = r2
            let a = topLeft - topRight
            let b = bottomRight - topRight
            let product = simd_cross(a, b)
            if product.z < 0 { topRight = prev }
        }
    }
    
    public var description: String {
        let string = """
        tl: \(tl) ... tr: \(tr)
        bl: \(bl) ... br: \(br)    
        """
        return string
    }
    
    public func draw(into renderer: Renderer) {
        let radius = 8.0
        renderer.setWidth(w: 1)
        renderer.setColor(color: .white)
        renderer.move(to: bl.cgpoint)
        renderer.circleAt(point: bl.cgpoint, radius: radius)
        renderer.move(to: tl.cgpoint)
        renderer.circleAt(point: tl.cgpoint, radius: radius)
        renderer.move(to: br.cgpoint)
        renderer.circleAt(point: br.cgpoint, radius: radius)
        renderer.move(to: tr.cgpoint)
        renderer.circleAt(point: tr.cgpoint, radius: radius)
        renderer.fill()
        renderer.move(to: bl.cgpoint)
        renderer.drawPath(points: [bl.cgpoint, tl.cgpoint])
        renderer.drawPath(points: [tl.cgpoint, tr.cgpoint])
        renderer.drawPath(points: [tr.cgpoint, br.cgpoint])
        renderer.drawPath(points: [br.cgpoint, bl.cgpoint])
    }
    
}
