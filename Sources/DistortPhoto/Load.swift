import CoreImage

struct Stickers {
    let images: [CIImage]
    let cgimages: [CGImage]
    init() {
        var text: String
        var url: URL
        var image: CIImage
        var images = [CIImage]()
        text = "\(LoadImages.Path.path)/fore0.jpg"
        url = URL(fileURLWithPath: text)
        image = CIImage(contentsOf: url)!
        images.append(image)
        text = "\(LoadImages.Path.path)/fore1.jpg"
        url = URL(fileURLWithPath: text)
        image = CIImage(contentsOf: url)!
        images.append(image)
        text = "\(LoadImages.Path.path)/fore2.jpg"
        url = URL(fileURLWithPath: text)
        image = CIImage(contentsOf: url)!
        images.append(image)
        self.images = images
        let context = CIContext()
        let maped = images.map { (ciimage) -> CGImage  in
            let cgimage = context.createCGImage(ciimage, from: ciimage.extent)!
            return cgimage
        }
        self.cgimages = maped
    }
}


struct LoadImages {
    
    struct Path {
        static var path: String {
            let dict = ProcessInfo.processInfo.environment
            let path = dict["HOME"]!
            return "\(path)/Desktop/TestImages/"
        }
    }
    
    func loadCIImage() -> (fore: CIImage, back: CIImage) {
        var text = "\(Path.path)/fore.jpg"
        var url = URL(fileURLWithPath: text)
        let photo = CIImage(contentsOf: url)
        text = "\(Path.path)/back.jpg"
        url = URL(fileURLWithPath: text)
        let back = CIImage(contentsOf: url)
        return (photo!, back!)
    }
    
    func loadCGImage() -> (fore: CGImage, back: CGImage) {
        let tuple = loadCIImage()
        let context = CIContext()
        let cgfore = context.createCGImage(tuple.fore, from: tuple.fore.extent)!
        let cgback = context.createCGImage(tuple.back, from: tuple.back.extent)!
        return (cgfore, cgback)
    }
    
}

struct Check {
    
    enum Planet { case mars, earth }
    func check() {
        let z = Planet.earth
        let m = Planet.mars
        if z == m {}
    }
    
}
