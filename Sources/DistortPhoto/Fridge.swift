import CoreImage
import Draw
import Math
import GridTools
import simd

public struct Fridge: Drawable {
    
    public enum Input {
        
        public enum Corner { case topleft, topright, bottomleft, bottomright }
        public enum Scale { case x, y, xy, scale }
        public enum Property { case position, rotation, scale(Scale) }
        public enum Target { case back(Property), fore(Property), scaletool(Property) }
        
        case freeCorner
        case corner(Corner)
        case transform(Target)
        case alpha

    }

    let context: CIContext
    public let back: CIImage
    let image: CIImage
    var transformedBack: CIImage
    var transformed: CIImage
    var transformedForLayer: CIImage
    
    public let viewport: SIMD2<Float>
    public var input: Input
    public var scroll: Scroll
    public var corners: Corners {
        didSet {
            frame.corners = corners
        }
    }
    public var transform: Transform {
        didSet {
            frame.brush.center = transform.getPositionTansformed()
            frame.brush.direction = transform.direction
            frame.matrix = transform.simd3Matrix
        }
    }
    
    var findAngel: FindAngel
    public var backcenter: Slim
    public var sticker: Slim {
        didSet {
            frame.brush = sticker
        }
    }
    public var frame: Frame
    public var rotationmarker: Slim
    
    public var track: Track
    var scaletrack: Track
    var scaleforetrack: Track
    var freetransformrack: Track
    var alpha: Float
    
    var foreCgImage: CGImage? { 
        let cgimage = context.createCGImage(transformed, from: transformed.extent)
        return cgimage
    }
    
    var backCgImage: CGImage? { 
        let cgimage = context.createCGImage(transformedBack, from: transformedBack.extent)
        return cgimage
    }
    
    public init(viewport diagonal: SIMD2<Float>, imagePack: (fore: CIImage, back: CIImage)) {
        context = CIContext()
        image = imagePack.fore
        back = imagePack.back
   
        input = .transform(.fore(.position))
        input = .freeCorner
        transformed = image
        transformedForLayer = transformed
        transformedBack = back
        let d = image.extent.size.diagonalFloat
        corners = Corners(diagonal: d)
        viewport = diagonal
        
        transform = Transform(image: d, viewport: viewport)
        scroll = Scroll(image: back.extent.size.diagonalFloat, viewport: diagonal)
        
        rotationmarker = Slim(center: viewport / 2, direction: transform.direction, oLongest: 50, oHiegher: 50, color: Color.fieldMarkup)
        rotationmarker.color = Color.deepyellow
        rotationmarker.strokeWidth = 4.0
        
        backcenter = rotationmarker
        backcenter.center = scroll.getPositionTansformed()
        backcenter.color = Color.black
        backcenter.strokeWidth = 3
        
        sticker = backcenter
        sticker.color = Color.bodypink
        sticker.strokeWidth = 1
        
        frame = Frame(brush: rotationmarker, diagonal: d)
        
        findAngel = FindAngel()
        track = Track()
        scaletrack = Track()
        scaleforetrack = Track()
        freetransformrack = Track()
        alpha = 1.0
        
        
        corners.matrix = transform.simd3Matrix
        
        adjustInitialScale()
        distort()
        transformBack()
    }
    
    public mutating func touchDownHandler(vector: SIMD2<Float>) {
        frame.first(touch: vector)
        switch frame.input {
        case .handler, .edge: input = .transform(.fore(.scale(.scale)))
        case .corner: input = .freeCorner
        case .none:
            switch input {
            case .transform(.back(.position)), .transform(.fore(.position)), .transform(.fore(.scale(.scale))): 
                switch check(screenTouch: vector) {
                case .back: input = .transform(.back(.position))
                case .fore: input = .transform(.fore(.position))
                }
            default: break
            }
        }
    }
    
    public mutating func touchDown(vector: SIMD2<Float>) {
        
        switch input {
        case .freeCorner: break
        default:
            frame.first(touch: vector)
            switch frame.input {
            case .none:
                switch input {
                case .transform(.back(.position)), .transform(.fore(.position)), .transform(.fore(.scale(.scale))): 
                    switch check(screenTouch: vector) {
                    case .back: input = .transform(.back(.position))
                    case .fore: input = .transform(.fore(.position))
                    }
                default: break
                }
            default: input = .transform(.fore(.scale(.scale)))
            }
        }
        
    }
    
    public enum TouchAt { case fore, back } 
    public func check(screenTouch: SIMD2<Float>) -> TouchAt {
        let result = transform.checkIntersect(screenTouch: screenTouch)
        return result ? .fore : .back
    }
    
    public struct Track {
        var previewTouch: SIMD2<Float>?
        var currentTouch: SIMD2<Float>?
        mutating func getDiff(touch: SIMD2<Float>) -> SIMD2<Float>? {
            previewTouch = currentTouch
            currentTouch = touch
            guard let cur = currentTouch, let prev = previewTouch else { return nil }
            let diff = cur - prev
            return diff
        }
        mutating func touchUp() {
            previewTouch = nil
            currentTouch = nil
        }
    }
    
    mutating func adjustInitialScale() {
        let kx = transform.scaleX / scroll.scale
        let ky = transform.scaleY / scroll.scale
        scroll.scale = scroll.initialscale
        sticker.center = scroll.getStickerTransformed()
        rotationmarker.center = sticker.center
        transform.setSticker(screenVector: sticker.center)
        transform.scaleX = scroll.scale * kx
        transform.scaleY = scroll.scale * ky
    }    
    
    public mutating func set(vector: SIMD2<Float>) {
        
        guard let dif = track.getDiff(touch: vector) else { return }
        
   
        switch input {
        case .alpha:
            alpha = vector.y / viewport.y
        case .freeCorner:
            guard let position = getCorner(by: vector) else { print("no corner"); break }
            corners.set(corner: vector, position: position)
        case .corner(let position):
            let target = vector
            corners.set(corner: target, position: position)
        case .transform(let target): 
            switch target {
            case .back(let property): 
                switch property {
                case .position: 
                    scroll.position += dif
                    backcenter.center = scroll.getPositionTansformed()                    
                    sticker.center = scroll.getStickerTransformed()
                    rotationmarker.center = sticker.center
                    transform.setSticker(screenVector: sticker.center)
                case .scale:
                    let f = dif.y / viewport.y
                    let out = 1 * f
                    let kx = transform.scaleX / scroll.scale
                    let ky = transform.scaleY / scroll.scale
                    scroll.scale += Float(out)
                    sticker.center = scroll.getStickerTransformed()
                    rotationmarker.center = sticker.center
                    transform.setSticker(screenVector: sticker.center)
                    transform.scaleX += Float(out) * kx
                    transform.scaleY += Float(out) * ky
                case .rotation: break
                }
            case .fore(let property):
                switch property {
                case .position:
                    transform.position += dif
                    sticker.center = transform.getPositionTansformed()
                    rotationmarker.center = sticker.center
                    scroll.setStickerCenter(screenVector: sticker.center)
                case .scale(let property):
                    let f = dif.y / viewport.y
                    let out = 1 * f
                    switch property {
                    case .x:
                        transform.scaleX += out
                    case .y:
                        transform.scaleY += out
                    case .xy:
                        let k = transform.scaleY / transform.scaleX 
                        transform.scaleX += out 
                        transform.scaleY += out * k
                    case .scale:
                        frame.touchInScreenSpace(vector: vector)
                        transform.scaleX *= frame.scaleX
                        transform.scaleY *= frame.scaleY
                        transform.rotateDirection(by: frame.alpha)
                    }
                case .rotation: 
                    rotationmarker.setWheel(coordinatesOnScreen: vector)
                    let alpha = findAngel.getAngel(slim: rotationmarker)
                    transform.rotateDirection(by: alpha)
                }
            case .scaletool(let property): 
                switch property {
                case .position: break
                case .scale: break
                case .rotation: break
                }
            }
            
        }
        
        corners.matrix = transform.simd3Matrix
    
        
        #if os(macOS)
        distort()
        transformBack()
        #endif
        
    }
    
    public func getCorner(by vector: SIMD2<Float>) -> Input.Corner? {
        let minimal: Float = 50
        if simd_distance(vector, corners.tl) < minimal { return .topleft }
        else if simd_distance(vector, corners.tr) < minimal { return .topright }
        else if simd_distance(vector, corners.bl) < minimal { return .bottomleft }
        else if simd_distance(vector, corners.br) < minimal { return .bottomright }
        else { return nil }
    }
    
    public mutating func touchup() {
        findAngel.touchUp()
        track.touchUp()
        scaletrack.touchUp()
        scaleforetrack.touchUp()
        frame.touchUp()
    }
    
    public mutating func adjustCornerCenter() {
        let prevposition = frame.centerScreen
        transform.centerModel = frame.centerModel
        corners.matrix = transform.simd3Matrix
        distort()
        let currentposition = frame.centerScreen
        let dif = currentposition - prevposition
        transform.position -= dif
        sticker.center = transform.getPositionTansformed()
        rotationmarker.center = sticker.center
        scroll.setStickerCenter(screenVector: sticker.center)
        corners.matrix = transform.simd3Matrix
        distort()
    }
    
    public mutating func setAlpha(alpha: Float) {
        self.alpha = alpha
        distortFolLayer()
    }
    
    public mutating func setBackPossition(offset: SIMD2<Float>) {
        scroll.position += offset
        backcenter.center = scroll.getPositionTansformed()                    
        sticker.center = scroll.getStickerTransformed()
        rotationmarker.center = sticker.center
        transform.setSticker(screenVector: sticker.center)
        corners.matrix = transform.simd3Matrix
    }
    
    public mutating func setBackScale(factor: Float) {
        guard let dif = scaletrack.getDiff(touch: [0, factor]) else { return }
        let f = dif.y / viewport.y
        let out = 1 * f
        let kx = transform.scaleX / scroll.scale
        let ky = transform.scaleY / scroll.scale
        scroll.scale += Float(out)
        sticker.center = scroll.getStickerTransformed()
        rotationmarker.center = sticker.center
        transform.setSticker(screenVector: sticker.center)
        transform.scaleX += Float(out) * kx
        transform.scaleY += Float(out) * ky
        corners.matrix = transform.simd3Matrix
    }
    
    public mutating func setForeScale(factor: Float) {
        guard let dif = scaleforetrack.getDiff(touch: [0, factor]) else { return }
        transform.scaleX += dif.y
        transform.scaleY += dif.y
        corners.matrix = transform.simd3Matrix
    }
    
    
    public mutating func setForePossition(offset: SIMD2<Float>) {
        transform.position += offset
        sticker.center = transform.getPositionTansformed()
        rotationmarker.center = sticker.center
        scroll.setStickerCenter(screenVector: sticker.center)
        corners.matrix = transform.simd3Matrix
    }
    
    public mutating func setFreeTransform(touchScreen: SIMD2<Float>) {
        frame.touchInScreenSpace(vector: touchScreen)
        transform.scaleX *= frame.scaleX
        transform.scaleY *= frame.scaleY
        transform.rotateDirection(by: frame.alpha)
        corners.matrix = transform.simd3Matrix
    }
    
    public mutating func animateToBounds() {
        
        if scroll.scale <= scroll.initialscale {
            scroll.position = .zero
            let kx = transform.scaleX / scroll.scale
            let ky = transform.scaleY / scroll.scale
            scroll.scale = scroll.initialscale
            sticker.center = scroll.getStickerTransformed()
            backcenter.center = scroll.getPositionTansformed()
            rotationmarker.center = sticker.center
            transform.setSticker(screenVector: sticker.center)
            transform.scaleX = scroll.scale * kx
            transform.scaleY = scroll.scale * ky
        } else {
            
            var deltaX: Float = 0
            var deltaY: Float = 0
            
       
            if scroll.screenCorners.bl.x > 0 { deltaX = -scroll.screenCorners.bl.x }
            else if scroll.screenCorners.br.x < viewport.x { deltaX = viewport.x - scroll.screenCorners.br.x }
            
            let currentheight = (scroll.screenCorners.tl - scroll.screenCorners.bl).y
            if currentheight < viewport.y {
                deltaY = -scroll.getPositionTansformed().y + (viewport / 2).y
            } else {
                if scroll.screenCorners.bl.y > 0 { deltaY = -scroll.screenCorners.bl.y  }
                else if scroll.screenCorners.tr.y < viewport.y { deltaY = viewport.y - scroll.screenCorners.tr.y }
            }
            

            let delta: SIMD2<Float> = [deltaX, deltaY]
            scroll.position += delta
            backcenter.center = scroll.getPositionTansformed()                    
            sticker.center = scroll.getStickerTransformed()
            rotationmarker.center = sticker.center
            transform.setSticker(screenVector: sticker.center)
            
        }
        

        corners.matrix = transform.simd3Matrix
        
        #if os(macOS)
        distort()
        transformBack()
        #endif
    }
    
    var viewrect: CGRect {
        let origin = CGPoint.zero
        let size = viewport.cgsize
        let rect = CGRect(origin: origin, size: size)
        return rect
    }
    
    public mutating func transformBack() {
        let filter = CIFilter(name: "CIAffineTransform")!
        filter.setValue(scroll.matrix, forKey: kCIInputTransformKey)        
        filter.setValue(back, forKey: kCIInputImageKey)
        
        let vector = CIVector(cgRect: viewrect)
        let crop = CIFilter(name: "CICrop")!
        crop.setValue(filter.outputImage!, forKey: kCIInputImageKey)
        crop.setValue(vector, forKey: "inputRectangle")
        
        transformedBack = crop.outputImage!
    }
    
    public mutating func distort() {
        
        let filterD = CIFilter(name: "CIPerspectiveTransform")!
        filterD.setValue(image, forKey: kCIInputImageKey)
        var cv = corners.bl.civector
        filterD.setValue(cv, forKey: "inputBottomLeft")
        cv = corners.tl.civector
        filterD.setValue(cv, forKey: "inputTopLeft")
        cv = corners.tr.civector
        filterD.setValue(cv, forKey: "inputTopRight")
        cv = corners.br.civector
        filterD.setValue(cv, forKey: "inputBottomRight")
        transformed = filterD.outputImage!
        
        distortFolLayer()

    }
    
    public var foremodel: DrawableCIImage {
        
        let rect = image.extent
        var origin = CGPoint(x: 0, y: 0)
        var size = rect.size

        let explorer = ImageLayerFrame(original: image, corners: corners)
        let tuple = explorer.getRect()
        size.width += CGFloat(-tuple.x * 2)
        origin.x -= CGFloat(-tuple.x)
        size.height += CGFloat(-tuple.y * 2)
        origin.y -= CGFloat(-tuple.y)
        
        let target = CGRect(origin: origin, size: size)
        let image = DrawableCIImage(image: transformedForLayer, context: context, frame: target)
        return image
    }
    
    public mutating func distortFolLayer() {

        
        let filterD = CIFilter(name: "CIPerspectiveTransform")!
        filterD.setValue(image, forKey: kCIInputImageKey)
        var cv = corners.bottomLeft.civector
        filterD.setValue(cv, forKey: "inputBottomLeft")
        cv = corners.topLeft.civector
        filterD.setValue(cv, forKey: "inputTopLeft")
        cv = corners.topRight.civector
        filterD.setValue(cv, forKey: "inputTopRight")
        cv = corners.bottomRight.civector
        filterD.setValue(cv, forKey: "inputBottomRight")
        
        let filterM = CIFilter(name: "CIColorMatrix")!
        filterM.setDefaults()
        let alphaVector = CIVector(x: 0, y: 0, z: 0, w: CGFloat(alpha))
        filterM.setValue(filterD.outputImage!, forKey: kCIInputImageKey)
        filterM.setValue(alphaVector, forKey: "inputAVector")
        
        transformedForLayer = filterM.outputImage! 
    } 
    
    var count = 0
    @available(iOS 11, *)    
    mutating func merge() {
        guard let space = back.colorSpace else { print("no space"); return }
        let result = getSaveImage()
        let string = "\(LoadImages.Path.path)/Results/result\(count).png"
        let url = URL(fileURLWithPath: string)
        let format = CIFormat.RGBA8
        do { try context.writePNGRepresentation(of: result, to: url, format: format, colorSpace: space, options: [:]) } 
        catch let error { print("save error: \(error)") }
        count += 1
    }
    
    
    func getSaveImage() -> CIImage {
        
        var savescroll = scroll
        var savetransform = transform
        var savecorners = corners
        var savesticker = sticker

        let kx = savetransform.scaleX / savescroll.scale
        let ky = savetransform.scaleY / savescroll.scale
        let dif = 1 - savescroll.scale
        savescroll.scale += dif
        savesticker.center = savescroll.getStickerTransformed()
        savetransform.scaleX += dif * kx
        savetransform.scaleY += dif * ky
        savetransform.setSticker(screenVector: savesticker.center)
        savecorners.matrix = savetransform.simd3Matrix
        
        let filterD = CIFilter(name: "CIPerspectiveTransform")!
        filterD.setValue(image, forKey: kCIInputImageKey)
        var cv = savecorners.bl.civector
        filterD.setValue(cv, forKey: "inputBottomLeft")
        cv = savecorners.tl.civector
        filterD.setValue(cv, forKey: "inputTopLeft")
        cv = savecorners.tr.civector
        filterD.setValue(cv, forKey: "inputTopRight")
        cv = savecorners.br.civector
        filterD.setValue(cv, forKey: "inputBottomRight")
    
        let filterM = CIFilter(name: "CIColorMatrix")!
        filterM.setDefaults()
        let alphaVector = CIVector(x: 0, y: 0, z: 0, w: CGFloat(alpha))
        filterM.setValue(filterD.outputImage!, forKey: kCIInputImageKey)
        filterM.setValue(alphaVector, forKey: "inputAVector")
        
        let filterB = CIFilter(name: "CIAffineTransform")!
        filterB.setValue(savescroll.matrix, forKey: kCIInputTransformKey)        
        filterB.setValue(back, forKey: kCIInputImageKey)
        
        let filter = CIFilter(name: "CISourceOverCompositing")!
        filter.setValue(filterM.outputImage!, forKey: "inputImage")
        filter.setValue(filterB.outputImage!, forKey: "inputBackgroundImage")
        
        let vector = CIVector(cgRect: filterB.outputImage!.extent)
        let crop = CIFilter(name: "CICrop")!
        crop.setValue(filter.outputImage!, forKey: kCIInputImageKey)
        crop.setValue(vector, forKey: "inputRectangle")
        
        let result = crop.outputImage!
        
        return result
    }
    
    public func getResultCGImageDefault() -> CGImage {
        let ci = getSaveImage()
        let def = context.createCGImage(ci, from: ci.extent)! 
        return def
    }
    
    public func getResultCGImageSetuped() -> CGImage {
        let space = back.colorSpace!
        let ci = getSaveImage()
        let setuped = context.createCGImage(ci, from: ci.extent, format: CIFormat.RGBA8, colorSpace: space)!
        return setuped
    }
    
    public func draw(into renderer: Renderer) {
        if let back = backCgImage {
            renderer.draw(image: back, in: transformedBack.extent)
        }
        if let fore = foreCgImage {
            renderer.draw(image: fore, in: transformed.extent)
        }
        
        
//        rotationmarker.draw(into: renderer)
//        sticker.draw(into: renderer)        
        frame.draw(into: renderer)
        backcenter.draw(into: renderer)
        corners.draw(into: renderer)
        
    }
    
}

public struct DrawableCIImage: Drawable {    
    let image: CIImage
    let context: CIContext
    let frame: CGRect
    
    public var cgImage: CGImage? { context.createCGImage(image, from: frame)  }
    
    public func draw(into renderer: Renderer) {
        guard let image = cgImage else { return }
        
        let origin = CGPoint(x: 50, y: 50)
        let f: CGFloat = 0.2
        let size = CGSize(width: frame.width * f, height: frame.height * f)
        let rect = CGRect(origin: origin, size: size)
        
        
        renderer.draw(color: Color.bodyrender, in: rect)
        renderer.draw(image: image, in: rect)
    }
    
}
