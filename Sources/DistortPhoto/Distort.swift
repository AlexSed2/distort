#if !os(iOS)
import MetalKit
import Draw
import App

@available(OSX 10.11, *)
public struct Setup {
    
    public init() {}
    
    public func prepareDraw() -> ControllerPacket {
        
        
        
        /// Вызывается в методе вью контроллера
        func initialdraw() -> (viewsWindowOne: [NSView], viewsWindowTwo: [NSView], viewsWindowThree: [NSView], stored: [Any]) {
            
            let canvas = Canvas(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 400, height: 700)))
            canvas.backcolor = Color.brown.cgcolor
            
            let canvastwo = DistortCanvas(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 400, height: 700)))
            canvastwo.backcolor = Color.darkbrown.cgcolor

            
            let canvasmark = Canvas(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 400, height: 700)))
            canvasmark.backcolor = CGColor.clear
            canvastwo.addSubview(canvasmark)
            
            
            let canvasthree = Canvas(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 700, height: 400)))
            canvasthree.backcolor = Color.darkbrown.cgcolor
            
            
            var model = Fridge(viewport: [400, 700], imagePack: LoadImages().loadCIImage()) 
            model.transform.scaleX = 0.2
            model.transform.scaleY = 0.2
            model.corners.matrix = model.transform.simd3Matrix
            model.distort()
            
            let loader = LoadImages().loadCGImage()
            var layers = Layers(backImage: loader.back, foreImage: loader.fore, viewport: [400, 700])
            layers.setBack(transform: model.scroll.matrix)
            layers.setFore(transform: model.transform.matrix)
            layers.renderToFore(image: model.foremodel.cgImage)
            
            // Left Screen
            canvas.setDrawables([model])
            
            canvas.mousedown = { point in
                model.touchDownHandler(vector: point.simd2float)
            }
            
            canvas.mousedrug = { point in
                
//                model.input = .transform(.back(.scale))
//                let track = model.track
//                model.set(vector: point.simd2float)
//                model.track = track
//                model.input = .transform(.back(.position))
//                model.set(vector: point.simd2float)
                
                model.set(vector: point.simd2float)
                
                canvas.setDrawables([model])
                layers.setBack(transform: model.scroll.matrix)
                layers.setFore(transform: model.transform.matrix)
                
                switch model.input {
                case .freeCorner, .corner, .alpha: layers.renderToFore(image: model.foremodel.cgImage)
                default: break
                }
                
                canvasthree.setDrawables([model.foremodel])
                canvasmark.setDrawables([model.corners])
            }
            
            canvas.mouseup = { point in                
                model.touchup()
                model.adjustCornerCenter()
                canvas.setDrawables([model])
                layers.setBack(transform: model.scroll.matrix)
                layers.setFore(transform: model.transform.matrix)
                canvasthree.setDrawables([model.foremodel])
                canvasmark.setDrawables([model.corners])
                
                model.animateToBounds()
                layers.setBackScroll(transform: model.scroll.matrix)
                layers.setForeScroll(transform: model.transform.matrix)
                canvas.setDrawables([model])
            }
            
            // Middle Screen
            let layer = CALayer()
            layer.frame = canvastwo.bounds
            canvastwo.layer = layer
            layers.install(to: canvastwo.layer!)
            
            
            canvasmark.mousedrug = { point in
                print("point: \(point)")
            }
            
            var previndex = 10
            func reloadImage() {
                let stickers = Stickers()
                var index = Int.random(in: 0...2)
                while index == previndex {
                    index = Int.random(in: 0...2)
                }
                previndex = index
                let stickerci = stickers.images[index]
                let stickercg = stickers.cgimages[index]
                let preview = model
                let new = Fridge(viewport: preview.viewport, imagePack: (fore: stickerci, back: preview.back))
                model = new
                model.transform.scaleX = 0.2
                model.transform.scaleY = 0.2
                model.corners.matrix = model.transform.simd3Matrix
                model.distort()
                
                canvas.setDrawables([model])
                canvasthree.setDrawables([model.foremodel])
                canvasmark.setDrawables([model.corners])
                
                layers.unistall()
                
                layers = Layers(backImage: loader.back, foreImage: stickercg, viewport: [400, 700])
                layers.setBack(transform: model.scroll.matrix)
                layers.setFore(transform: model.transform.matrix)
                layers.install(to: canvastwo.layer!)
                
            }
            
            defer {
                DispatchQueue.global().async {
                    var command = ""
                    while command != "end" {
                        print("input command:")
                        guard let line = readLine() else { continue }
                        switch line {
                        case "p": model.input = .transform(.fore(.position))
                        case "tl": model.input = .corner(.topleft)
                        case "tr": model.input = .corner(.topright)
                        case "bl": model.input = .corner(.bottomleft)
                        case "br": model.input = .corner(.bottomright)
                        case "r": model.input = .transform(.fore(.rotation))
                        case "s": model.input = .transform(.fore(.scale(.xy)))
                        case "sx": model.input = .transform(.fore(.scale(.x)))
                        case "sy": model.input = .transform(.fore(.scale(.y)))
                        case "bp": model.input = .transform(.back(.position))
                        case "bs": model.input = .transform(.back(.scale(.xy)))
                        case "mp": model.input = .transform(.scaletool(.position))
                        case "fc": model.input = .freeCorner
                        case "scale": model.input = .transform(.fore(.scale(.scale)))
                        case "save": model.merge()
                        case "reload", "n": DispatchQueue.main.async { reloadImage() }
                        case "alpha": model.input = .alpha
                        default: break
                        }
                        command = line
                    }
                    print("end commands")
                }
            }
            
            return ([canvas], [canvastwo], [canvasthree], [1])
        }
        
        
        /// Вызывается в методе вью контроллера
        func system(a: Int) {}
        
        
        let packet = ControllerPacket(initialClosure: initialdraw, systemCallClosure: system)
        return packet
    }
}
#endif
