import QuartzCore
import Draw

public struct Layers {
    
    var fore: CALayer
    let back: CALayer
    var foresize: SIMD2<Float>
    let backsize: SIMD2<Float>
    let viewport: SIMD2<Float>
    let backanimator: CABasicAnimation
    let foreanimator: CABasicAnimation
    
    
    let foreanimatorScroll: CABasicAnimation
    let backanimatorScroll: CABasicAnimation 
    var prevforematrix = CGAffineTransform.identity
    var prevbackmatrix = CGAffineTransform.identity
    
    public init(backImage: CGImage, foreImage: CGImage, viewport: SIMD2<Float>) {
        
        back = CALayer()
        let backsize = CGSize(width: backImage.width, height: backImage.height)
        self.backsize = backsize.diagonalFloat
        back.frame = CGRect(origin: .zero, size: backsize)
        back.contents = backImage
        back.anchorPoint = CGPoint(x: 0.0, y: 0.0)
        
        fore = CALayer()
        let foresize = CGSize(width: foreImage.width, height: foreImage.height)
        self.foresize = foresize.diagonalFloat
        fore.frame = CGRect(origin: .zero, size: foresize)
        fore.contents = foreImage
        fore.anchorPoint = CGPoint(x: 0.0, y: 0.0)
        fore.contentsGravity = .center

//        back.borderColor = Color.bodycian.cgcolor
//        back.borderWidth = 5
//        
//        fore.borderColor = Color.bodycian.cgcolor
//        fore.borderWidth = 5
        
        back.position = .zero
        fore.position = .zero
        
        backanimator = CABasicAnimation(keyPath: "transform")
        foreanimator = CABasicAnimation(keyPath: "transform")
        backanimator.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        foreanimator.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        backanimator.duration = 0
        foreanimator.duration = 0
        
        backanimatorScroll = CABasicAnimation(keyPath: "transform")
        foreanimatorScroll = CABasicAnimation(keyPath: "transform")
        backanimatorScroll.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        foreanimatorScroll.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        backanimatorScroll.duration = CFTimeInterval(0.25)
        foreanimatorScroll.duration = CFTimeInterval(0.25)
        
        self.viewport = viewport
    }
    
    public func renderToFore(image: CGImage?) {
        guard let image = image else { return }
        fore.contents = image
    }
    
    
    func modifyToIOS(matrix: inout CGAffineTransform) {
        let translate = CGAffineTransform(translationX: 0, y: CGFloat(viewport.y))
        let scale = CGAffineTransform(scaleX: 1, y: -1)
        let xform = scale.concatenating(translate)
        matrix = matrix.concatenating(xform)
    }
    
    
    func makeTransforms(from affine: CGAffineTransform) -> CATransform3D {
        CATransform3DMakeAffineTransform(affine)
    }
    
    
    public mutating func setFore(transform matrix: CGAffineTransform) {
        var matrix = matrix
        modifyToIOS(matrix: &matrix)
        
        
        let scalematrix = CGAffineTransform(scaleX: 1, y: -1)
        let translate = CGAffineTransform(translationX: 0, y: CGFloat(foresize.y))
        let xform = scalematrix.concatenating(translate)
        matrix = xform.concatenating(matrix)
        
//        foreanimator.fromValue = makeTransforms(from: prevforematrix)
        foreanimator.toValue = makeTransforms(from: matrix)
        prevforematrix = matrix
        
        fore.transform = makeTransforms(from: matrix)
        fore.add(foreanimator, forKey: "transform")
        

    }
    
    
    public mutating func setBack(transform matrix: CGAffineTransform) {
        
        var matrix = matrix
        modifyToIOS(matrix: &matrix)
        
        let scalematrix = CGAffineTransform(scaleX: 1, y: -1)
        let translate = CGAffineTransform(translationX: 0, y: CGFloat(backsize.y))
        let xform = scalematrix.concatenating(translate)
        matrix = xform.concatenating(matrix)
        
//        backanimator.fromValue = makeTransforms(from: prevbackmatrix)
        backanimator.toValue = makeTransforms(from: matrix)
        prevbackmatrix = matrix
        
        back.transform = makeTransforms(from: matrix)
        back.add(backanimator, forKey: "transform")
        
    }
    
 
    public mutating func setForeScroll(transform matrix: CGAffineTransform) {
        var matrix = matrix
        modifyToIOS(matrix: &matrix)
        
        
        let scalematrix = CGAffineTransform(scaleX: 1, y: -1)
        let translate = CGAffineTransform(translationX: 0, y: CGFloat(foresize.y))
        let xform = scalematrix.concatenating(translate)
        matrix = xform.concatenating(matrix)
        
        foreanimatorScroll.fromValue = makeTransforms(from: prevforematrix)
        foreanimatorScroll.toValue = makeTransforms(from: matrix)
        prevforematrix = matrix
        
        fore.add(foreanimatorScroll, forKey: "transform")
        fore.transform = makeTransforms(from: matrix)
        
        
    }
    

    public mutating func setBackScroll(transform matrix: CGAffineTransform) {
        
        var matrix = matrix
        modifyToIOS(matrix: &matrix)
        
        let scalematrix = CGAffineTransform(scaleX: 1, y: -1)
        let translate = CGAffineTransform(translationX: 0, y: CGFloat(backsize.y))
        let xform = scalematrix.concatenating(translate)
        matrix = xform.concatenating(matrix)
        
        backanimatorScroll.fromValue = makeTransforms(from: prevbackmatrix)
        backanimatorScroll.toValue = makeTransforms(from: matrix)
        prevbackmatrix = matrix
        
        back.add(backanimatorScroll, forKey: "transform")
        back.transform = makeTransforms(from: matrix)
        
    }
    
    public func install(to layer: CALayer) {
        layer.addSublayer(back)
        layer.addSublayer(fore)
    }
    
    public mutating func reinstall(foreImage: CGImage, layer: CALayer) {
        fore.removeFromSuperlayer()
        
        fore = CALayer()
        let foresize = CGSize(width: foreImage.width, height: foreImage.height)
        self.foresize = foresize.diagonalFloat
        fore.frame = CGRect(origin: .zero, size: foresize)
        fore.contents = foreImage
        fore.anchorPoint = CGPoint(x: 0.0, y: 0.0)
        fore.contentsGravity = .center
        layer.addSublayer(fore)
    }
    
    public mutating func unistall() {
        fore.removeFromSuperlayer()
        back.removeFromSuperlayer()
    }
}
