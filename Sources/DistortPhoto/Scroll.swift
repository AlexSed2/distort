import CoreGraphics
import simd
import Draw

public struct Scroll: Drawable {
    
    let viewport: SIMD2<Float>
    let image: SIMD2<Float>
    var position: SIMD2<Float>
    var centrolized: SIMD2<Float>
    var stickerCenter: SIMD2<Float>
    let corners: Corners
    public var scale: Float
    
    var initialscale: Float { viewport.x / image.x }
    
    var screenCorners: Corners {
        var target = corners
        target.matrix = matrix.simd
        return target
    }
    
    init(image: SIMD2<Float>, viewport: SIMD2<Float>) {
        self.viewport = viewport
        self.image = image
        self.position = .zero
        self.scale = 1.0
        self.centrolized = -image / 2
        corners = Corners(diagonal: image)
        stickerCenter = -centrolized
    }
    
    
    
    // Marker
    func getPositionTansformed() -> SIMD2<Float> {
        let s3 = SimdConvertor().convertTo3(simd2: image / 2)
        let maped = s3 * matrix.simd
        let s2 = SimdConvertor().convertTo2(simd3: maped)
        return s2
    }
    
    // Sticker
    mutating func setStickerCenter(screenVector: SIMD2<Float>) {
        let s3 = SimdConvertor().convertTo3(simd2: screenVector)
        let model = s3 * matrix.simd.inverse
        let s2 = SimdConvertor().convertTo2(simd3: model)
        stickerCenter = s2
    }

    func getStickerTransformed() -> SIMD2<Float> {
        let s3 = SimdConvertor().convertTo3(simd2: stickerCenter)
        let maped = s3 * matrix.simd
        let s2 = SimdConvertor().convertTo2(simd3: maped)
        return s2
    }
    
    var centrolizedMatrix: CGAffineTransform { CGAffineTransform(translationX: CGFloat(centrolized.x), y: CGFloat(centrolized.y)) }
    var scaleMatrix: CGAffineTransform { CGAffineTransform(scaleX: CGFloat(scale), y: CGFloat(scale)) }
    var translateMatrix: CGAffineTransform { CGAffineTransform(translationX: CGFloat(position.x), y: CGFloat(position.y)) } 
    var screenCenterMatrix: CGAffineTransform { 
        let center = viewport / 2
        return CGAffineTransform(translationX: CGFloat(center.x), y: CGFloat(center.y))  
    } 
    public var matrix: CGAffineTransform {        
        return centrolizedMatrix
            .concatenating(scaleMatrix)
            .concatenating(screenCenterMatrix)
            .concatenating(translateMatrix)
    }
    
    public func draw(into renderer: Renderer) {
        screenCorners.draw(into: renderer)
    }
}
