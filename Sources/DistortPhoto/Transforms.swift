import CoreGraphics
import simd
import Math

public struct Transform {
    
    var centrolized: SIMD2<Float>
    var position: SIMD2<Float>
    public var scaleX: Float
    public var scaleY: Float
    var direction: SIMD2<Float>
    var screen: SIMD2<Float>
    var image: SIMD2<Float>
    var centerModel: SIMD2<Float>
    
    init(image diagonal: SIMD2<Float>, viewport: SIMD2<Float>) {
        position = .zero
        scaleX = 1
        scaleY = 1
        direction = [1, 0]
        centrolized = -diagonal / 2
        screen = viewport
        image = diagonal
        centerModel = diagonal / 2
    }
    
    public func checkIntersect(screenTouch: SIMD2<Float>) -> Bool {
        let s3 = SimdConvertor().convertTo3(simd2: screenTouch)
        let r3 = s3 * matrix.simd.inverse
        let r2 = SimdConvertor().convertTo2(simd3: r3) // model space
        let vector = r2
        let rect = CGRect(origin: .zero, size: image.cgsize)
        let intersect = rect.contains(vector.cgpoint)
        return intersect
    }
    
    mutating func rotateDirection(by angel: Float) {
        guard angel.isNaN == false else { return }
        let matrix = Matricies().rotation2x2(angle: angel)
        direction = direction * matrix
    }
    
    // Marker
    func getPositionTansformed() -> SIMD2<Float> {
//        let s3 = SimdConvertor().convertTo3(simd2: image / 2)
        let s3 = SimdConvertor().convertTo3(simd2: centerModel)
        let maped = s3 * matrix.simd
        let s2 = SimdConvertor().convertTo2(simd3: maped)
        return s2
    }
    
    mutating func setSticker(screenVector: SIMD2<Float>) {
        position = screenVector - screen / 2
    }
    
    var scaleMatrix: CGAffineTransform {
        return CGAffineTransform(scaleX: CGFloat(scaleX), y: CGFloat(scaleY)) 
    }
    
    var rotateMatrix: CGAffineTransform { 
        let normalized = direction
        guard normalized.x != 0 else { return CGAffineTransform.identity }
        let tana = normalized.y / normalized.x
        var a = atan(tana)
        if normalized.x < 0 && normalized.y > 0 { a += Float.pi }
        if normalized.x < 0 && normalized.y < 0 { a += Float.pi }
        let m = CGAffineTransform(rotationAngle: CGFloat(a))
        return m
    }
    
    var centrolizedMatrix: CGAffineTransform {
        let offset = -centerModel
        return CGAffineTransform(translationX: CGFloat(offset.x), y: CGFloat(offset.y)) 
    }
    
    
    var screenCenterMatrix: CGAffineTransform { 
        let center = screen / 2
        return CGAffineTransform(translationX: CGFloat(center.x), y: CGFloat(center.y))  
    } 
    
    var translateMatrix: CGAffineTransform { 
        CGAffineTransform(translationX: CGFloat(position.x), y: CGFloat(position.y)) 
    }
    
    public var matrix: CGAffineTransform {         
        return centrolizedMatrix
            .concatenating(scaleMatrix)
            .concatenating(rotateMatrix)
            .concatenating(screenCenterMatrix)
            .concatenating(translateMatrix)
    }
    public var simd3Matrix: simd_float3x3 { ConvertMatrix().makeSimd(from: matrix) }
    
}


struct ConvertMatrix {
    func makeSimd(from cgmatrix: CGAffineTransform) -> simd_float3x3 {
        var m3 = matrix_identity_float3x3
        m3[0, 0] = Float(cgmatrix.a)
        m3[1, 0] = Float(cgmatrix.b)
        m3[0, 1] = Float(cgmatrix.c)
        m3[1, 1] = Float(cgmatrix.d)
        m3[0, 2] = Float(cgmatrix.tx)
        m3[1, 2] = Float(cgmatrix.ty)
        return m3
    }
}
