#if !os(iOS)
import App

struct Scene {
    func run() {
        runApp()
    }
    func runApp() {
        let packet = Setup().prepareDraw()
        App(packet: packet).run()        
    }
    func testFridge() {
    }

}
#endif
