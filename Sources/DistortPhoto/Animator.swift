struct Animator {
    
    var animatedModel: Fridge?
    let animationClojure: (Fridge) -> Void
    
    init(animation: @escaping (Fridge) -> Void) {
        animationClojure = animation
    }
    
    mutating func animate(model: Fridge) {
        animatedModel = model
        // mutating cycle of animatedModel 
        animationClojure(animatedModel!)
    }
}
