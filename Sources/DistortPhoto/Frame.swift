import GridTools
import Draw
import simd
import CoreGraphics

public struct Frame: Drawable {    
    
    public enum Input { 
        public enum Edge { case lox, rox, toy, boy }
        public enum Corner { case bl, tl, tr, br }
        case edge(Edge), corner(Corner), handler, none
    }
    
    
    public var input: Input
    
    var brush: Slim
    var findAngel: FindAngel
    
    let diagonal: SIMD2<Float>
    
    // Левый нижний угол в модел спейс находится в 0, 0
    
    var bottomLeft: SIMD2<Float>
    var topLeft: SIMD2<Float>
    var topRight: SIMD2<Float>
    var bottomRight: SIMD2<Float>
    var corners: Corners
    var matrix: simd_float3x3
    
    var rectcolor: Color
    var middlecolor: Color
    
    
    // Output
    var scaleX: Float 
    var scaleY: Float
    var alpha: Float
     
    
    var handler: SIMD2<Float> {
        let vector = corners.topRight - corners.topLeft        
        let z: SIMD2<Float> = [vector.y, -vector.x]
        var k = corners.topLeft + vector / 2
        k += -simd_normalize(z) * (topRight.y / 3) 
        return k
    }
    
    var handlerScreen: SIMD2<Float> {
        let s3 = SimdConvertor().convertTo3(simd2: handler)
        let r3 = s3 * matrix
        return SimdConvertor().convertTo2(simd3: r3)
    }
    
    var lox: SIMD2<Float> {
        var dif = corners.topLeft - corners.bottomLeft
        dif /= 2
        return corners.bottomLeft + dif        
    }
    var rox: SIMD2<Float> {
        var dif = corners.topRight - corners.bottomRight
        dif /= 2
        return corners.bottomRight + dif
    }
    var toy: SIMD2<Float> {
        var dif = corners.topRight - corners.topLeft
        dif /= 2
        return corners.topLeft + dif
    }
    var boy: SIMD2<Float> {
        var dif = corners.bottomRight - corners.bottomLeft
        dif /= 2
        return corners.bottomLeft + dif
    }
    
    
    var loxMscreen: SIMD2<Float> {
        let s3 = SimdConvertor().convertTo3(simd2: lox)
        let r3 = s3 * matrix
        return SimdConvertor().convertTo2(simd3: r3)
    }
    
    var roxMscreen: SIMD2<Float> {
        let s3 = SimdConvertor().convertTo3(simd2: rox)
        let r3 = s3 * matrix
        return SimdConvertor().convertTo2(simd3: r3)
    }
    
    var toyMscreen: SIMD2<Float> {
        let s3 = SimdConvertor().convertTo3(simd2: toy)
        let r3 = s3 * matrix
        return SimdConvertor().convertTo2(simd3: r3)
    }
    
    var boyMscreen: SIMD2<Float> {
        let s3 = SimdConvertor().convertTo3(simd2: boy)
        let r3 = s3 * matrix
        return SimdConvertor().convertTo2(simd3: r3)
    }
    
    var centerModel: SIMD2<Float> {
        let vector = (rox - lox) / 2
        return lox + vector
    }
    
    var centerScreen: SIMD2<Float> {
        let s3 = SimdConvertor().convertTo3(simd2: centerModel)
        let r3 = s3 * matrix
        return SimdConvertor().convertTo2(simd3: r3)
    }
    
    init(brush: Slim, diagonal: SIMD2<Float>) {
        var brush = brush
        brush.strokeWidth = 8
        brush.color = .bodycian
        self.brush = brush
        
        bottomLeft = .zero
        topLeft = bottomLeft + [0, diagonal.y]
        topRight = topLeft + [diagonal.x, 0]
        bottomRight = bottomLeft + [diagonal.x, 0]

        
        self.diagonal = diagonal
        
        matrix = matrix_identity_float3x3
        rectcolor = Color.white
        middlecolor = Color.white
        
        input = .none
        
        findAngel = FindAngel()
        
        alpha = 0
        
        scaleX = 1
        scaleY = 1
        
        corners = Corners(diagonal: diagonal)
    }
    
    public mutating func first(touch vector: SIMD2<Float>) {
        let s3 = SimdConvertor().convertTo3(simd2: vector)
        let r3 = s3 * matrix.inverse
        let r2 = SimdConvertor().convertTo2(simd3: r3) // model space
        let vector = r2
        
        let minimal: Float = 70 // * max(matrix[0, 0], matrix[1, 1])
        
        
        if simd_distance(vector, lox) < minimal {  input = .edge(.lox) } 
        else if simd_distance(vector, rox) < minimal { input = .edge(.rox) } 
        else if simd_distance(vector, toy) < minimal { input = .none } 
        else if simd_distance(vector, boy) < minimal { input = .edge(.boy) }
        else if simd_distance(vector, corners.bottomLeft) < minimal { input = .corner(.bl) }
        else if simd_distance(vector, corners.topLeft) < minimal { input = .corner(.tl) }
        else if simd_distance(vector, corners.topRight) < minimal { input = .corner(.tr) }
        else if simd_distance(vector, corners.bottomRight) < minimal { input = .corner(.br) }
        else if simd_distance(vector, handler) < minimal { input = .handler }
        else { input = .none }
        
    }
    

    
    public mutating func touchInScreenSpace(vector: SIMD2<Float>) {
        
        let v = vector
        
        let s3 = SimdConvertor().convertTo3(simd2: vector)
        let r3 = s3 * matrix.inverse
        let r2 = SimdConvertor().convertTo2(simd3: r3) // model space
        let vector = r2
        
        switch input {
        case .edge(let edge):
            switch edge {
            case .rox: 
                scaleX = vector.x / rox.x
            case .lox:
                let z = topRight.x - (vector.x - lox.x)
                scaleX = z / topRight.x
            case .toy:
                scaleY = vector.y / toy.y
            case .boy:
                let z = topRight.y - (vector.y - boy.y)
                scaleY = z / topRight.y
            }
        case .corner(let corner):
            let k = scaleX / scaleY
            switch corner {
            case .br: 
                scaleX = vector.x / corners.bottomRight.x
            case .tr: 
                scaleX = vector.x / corners.topRight.x
            case .bl: 
                let z = bottomRight.x - (vector.x - corners.bottomLeft.x)
                scaleX = z / bottomRight.x
            case .tl: 
                let z = bottomRight.x - (vector.x - corners.topLeft.x)
                scaleX = z / bottomRight.x
            }
            scaleY = scaleX * k
            brush.setWheel(coordinatesOnScreen: v)
            alpha = findAngel.getAngel(slim: brush)
        case .handler: 
            let k = scaleY / scaleX
            let dif = corners.topRight - handler
            let target = vector + dif
            scaleY = target.y / corners.topRight.y
            scaleX = scaleY * k
            brush.setWheel(coordinatesOnScreen: v)
            alpha = findAngel.getAngel(slim: brush)
            
        case .none: break
        }
    
    }
    
    mutating func touchUp() {
        input = .none
        scaleX = 1
        scaleY = 1
        alpha = 0
        findAngel.touchUp()
    }
    
    public func draw(into renderer: Renderer) {
        let size: SIMD2<Float> = [10, 10]
        var rect = CGRect(origin: .zero, size: size.cgsize)
        
        rect.origin = (loxMscreen - size / 2).cgpoint
        renderer.draw(color: middlecolor, in: rect)
        
        rect.origin = (roxMscreen - size / 2).cgpoint
        renderer.draw(color: middlecolor, in: rect)
        
        rect.origin = (boyMscreen - size / 2).cgpoint
        renderer.draw(color: middlecolor, in: rect)
        
        rect.origin = (centerScreen - size / 2).cgpoint
        renderer.draw(color: middlecolor, in: rect)
        
        let radius = 8.0
        renderer.move(to: handlerScreen.cgpoint)
        renderer.circleAt(point: handlerScreen.cgpoint, radius: radius)
        renderer.fill()
        
        renderer.setWidth(w: 1)
        renderer.setColor(color: .white)
        renderer.move(to: toyMscreen.cgpoint)
        renderer.drawPath(points: [handlerScreen.cgpoint, toyMscreen.cgpoint])
    }
}
