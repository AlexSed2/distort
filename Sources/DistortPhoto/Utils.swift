import CoreImage
import simd
import App

extension SIMD2 where Scalar: BinaryFloatingPoint {
    var civector: CIVector { CIVector(cgPoint: cgpoint) }
}

struct SimdConvertor {
    func convertTo2(simd3: SIMD3<Float>) -> SIMD2<Float> { [simd3.x, simd3.y] }
    func convertTo3(simd2: SIMD2<Float>) -> SIMD3<Float> { [simd2.x, simd2.y, 1] }
}

extension CGAffineTransform {
    var simd: simd_float3x3 { ConvertMatrix().makeSimd(from: self) }
}

#if os(macOS)
class DistortCanvas: Canvas {
    override var isFlipped: Bool { true }
}
#endif
