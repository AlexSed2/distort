import simd

public func capRectangel(edge: 
    (firstbottom: SIMD3<Float>, 
    lastbottom: SIMD3<Float>,
    firsttop: SIMD3<Float>,
    lasttop: SIMD3<Float>)) -> [(vertex: SIMD3<Float>, normal: SIMD3<Float>)] 
{
    let triangels = makeTwoTriangelsFrom(edge: edge)
    let bottomtriangel = triangels.bottom
    let bottomnormal = getNormalOf(triangel: bottomtriangel)
    let toptriangel = triangels.top
    let topnormal = getNormalOf(triangel: toptriangel)
    
    return bottomtriangel.map { ($0, bottomnormal) } + toptriangel.map { ($0, topnormal) }
}
