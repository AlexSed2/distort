// https://www.graphicon.ru/oldgr/courses/cg99/notes/lect9/transform/trans_deriv.htm

import CoreGraphics
import simd

public struct Matricies {
    
    public init() {}
    
    public func updateModelToAspect(size: CGSize) -> matrix_float3x3 {
        let aspect = Float(size.height / size.width)
        let aspectihat: SIMD3<Float> = [1, 0, 0]
        let aspectjhat: SIMD3<Float> = [1, aspect, 0]
        let aspectkhat: SIMD3<Float> = [0, 0, 1]
        let aspectmatrix = matrix_float3x3(aspectihat, aspectjhat, aspectkhat)
        return aspectmatrix
    }
    
    public func scaleXY(times: Float) -> matrix_float3x3 {
        let ihat: SIMD3<Float> = [times, 0, 0]
        let jhat: SIMD3<Float> = [0, times, 0]
        let khat: SIMD3<Float> = [0, 0, 1]
        let matrix = matrix_float3x3(ihat, jhat, khat)
        return matrix
    }
    
    public func translate(by vector: SIMD2<Float>) -> simd_float3x3 {
        var matrix = matrix_identity_float3x3
        matrix[0, 2] = vector.x
        matrix[1, 2] = vector.y
        return matrix
    }
    
    public func translate4x4(by vector: SIMD2<Float>) -> simd_float4x4 {
        var matrix = matrix_identity_float4x4
        matrix[3, 0] = vector.x
        matrix[3, 1] = vector.y
        return matrix
    }
    
    public func translate4x4d(by vector: SIMD2<Double>) -> simd_double4x4 {
        var matrix = matrix_identity_double4x4
        matrix[3, 0] = vector.x
        matrix[3, 1] = vector.y
        return matrix
    }
    
    public func translate4x4(by vector: SIMD3<Float>) -> simd_float4x4 {
        var matrix = matrix_identity_float4x4
        matrix[3, 0] = vector.x
        matrix[3, 1] = vector.y
        matrix[3, 2] = vector.z
        return matrix
    }
    
    public func translate4x4(by vector: SIMD3<Double>) -> simd_double4x4 {
        var matrix = matrix_identity_double4x4
        matrix[3, 0] = vector.x
        matrix[3, 1] = vector.y
        matrix[3, 2] = vector.z
        return matrix
    }
    
    public func scale4x4(by factor: Float) -> simd_float4x4 {
        var matrix = matrix_identity_float4x4
        matrix[0, 0] = factor
        matrix[1, 1] = factor
        matrix[2, 2] = factor
        return matrix
    }
    
    public func scale4x4(by factor: Double) -> simd_double4x4 {
        var matrix = matrix_identity_double4x4
        matrix[0, 0] = factor
        matrix[1, 1] = factor
        matrix[2, 2] = factor
        return matrix
    }
    
    public func centerModel(modelwidth: Float, modelheight: Float, modelorigin: SIMD2<Float>) -> simd_float3x3  {
        let fromOriginToCenterOfModel = SIMD2<Float>([modelwidth / 2, modelheight / 2])
        let translateone = translate(by: -modelorigin - fromOriginToCenterOfModel)
        return translateone
    }
    
    public func transform3x3to4x4(from matrix: simd_float3x3) -> simd_float4x4 {
        
        func transform(vector: SIMD3<Float>, addon: Float) -> SIMD4<Float> {
            return SIMD4<Float>([vector.x, vector.y, vector.z, addon])
        }
        
        let first = transform(vector: matrix[0], addon: 0)
        let second = transform(vector: matrix[1], addon: 0)
        let third = transform(vector: matrix[2], addon: 0)
        let fourth = SIMD4<Float>([0, 0, 0, 1])
        
        return simd_float4x4([first, second, third, fourth])
    }
    
    public func rotation3x3(angle: Float) -> simd_float3x3 {
        let rows = [
            simd_float3( cos(angle), sin(angle), 0),
            simd_float3(-sin(angle), cos(angle), 0),
            simd_float3( 0,          0,          1)
        ]
        
        return float3x3(rows: rows)
    }
    
    public func rotation3x3(angle: Double) -> simd_double3x3 {
        let rows = [
            simd_double3( cos(angle), sin(angle), 0),
            simd_double3(-sin(angle), cos(angle), 0),
            simd_double3( 0,          0,          1)
        ]
        
        return simd_double3x3(rows: rows)
    }
    
    public func rotation2x2(angle: Double) -> simd_double2x2 {
        let ihat: SIMD2<Double> = [cos(angle), sin(angle)]
        let jhat: SIMD2<Double> = [-sin(angle), cos(angle)]
        let matrix = matrix_double2x2([ihat, jhat])
        return matrix
    }
    
    public func rotation2x2(angle: Float) -> simd_float2x2 {
        let ihat: SIMD2<Float> = [cos(angle), sin(angle)]
        let jhat: SIMD2<Float> = [-sin(angle), cos(angle)]
        let matrix = matrix_float2x2([ihat, jhat])
        return matrix
    }
    
    public func rotateZ(angel: Float) -> simd_float4x4 {
        var matrix = matrix_identity_float4x4
        matrix[0, 0] =  cos(angel)
        matrix[1, 0] = -sin(angel)
        matrix[0, 1] =  sin(angel)
        matrix[1, 1] =  cos(angel)
        return matrix
    }
    
    public func rotateX(angel: Float) -> simd_float4x4 {
        var matrix = matrix_identity_float4x4
        matrix[1, 1] =  cos(angel)
        matrix[2, 1] = -sin(angel)
        matrix[1, 2] =  sin(angel)
        matrix[2, 2] =  cos(angel)
        return matrix
    }
    
    public func rotateY(angel: Float) -> simd_float4x4 {
        var matrix = matrix_identity_float4x4
        matrix[0, 0] =  cos(angel)
        matrix[0, 2] =  sin(angel)
        matrix[2, 0] = -sin(angel)
        matrix[2, 2] =  cos(angel)
        return matrix
    }
    
    public func perspective(perspectiveWithAspect aspect: Float, fovy: Float, near: Float, far: Float) -> simd_float4x4 {
        let yScale = 1 / tan(fovy * 0.5)
        let xScale = yScale / aspect
        let zRange = far - near
        let zScale = -(far + near) / zRange
        let wzScale = -2 * far * near / zRange
        
        
        // List of the matrix' columns
        let vectorP: SIMD4<Float> = [xScale,      0,       0,  0]
        let vectorQ: SIMD4<Float> = [     0, yScale,       0,  0]
        let vectorR: SIMD4<Float> = [     0,      0,  zScale, -1]
        let vectorS: SIMD4<Float> = [     0,      0, wzScale,  0]
        
        return simd_float4x4(columns: (vectorP, vectorQ, vectorR, vectorS))
        
    }
    
    public func perspective(perspectiveWithAspect aspect: Double, fovy: Double, near: Double, far: Double) -> simd_double4x4 {
        let yScale = 1 / tan(fovy * 0.5)
        let xScale = yScale / aspect
        let zRange = far - near
        let zScale = -(far + near) / zRange
        let wzScale = -2 * far * near / zRange
        
        
        // List of the matrix' columns
        let vectorP: SIMD4<Double> = [xScale,      0,       0,  0]
        let vectorQ: SIMD4<Double> = [     0, yScale,       0,  0]
        let vectorR: SIMD4<Double> = [     0,      0,  zScale, -1]
        let vectorS: SIMD4<Double> = [     0,      0, wzScale,  0]
        
        return simd_double4x4(columns: (vectorP, vectorQ, vectorR, vectorS))
        
    }
    
    public func zoomMatrix(vector: SIMD3<Float>) -> simd_float4x4 {
        let baseX: SIMD4<Float> = [1, 0, 0, 0]
        let baseY: SIMD4<Float> = [0, 1, 0, 0]
        let baseZ: SIMD4<Float> = [0, 0, 1, 0]
        let baseW: SIMD4<Float> = [vector.x, vector.y, vector.z, 1]
        return simd_float4x4(columns: (baseX, baseY, baseZ, baseW))
    }
}
