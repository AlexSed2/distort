public func rightRoundNeighborOnBaseTen(number: Int) -> Int {
    switch number {
    case let x where   (0..<5).contains(x): return 5
    case let x where  (5..<10).contains(x): return 10
    case let x where (10..<15).contains(x): return 15
    case let x where (15..<20).contains(x): return 20
    case let x where (20..<25).contains(x): return 25
    case let x where (25..<30).contains(x): return 30
    case let x where (30..<35).contains(x): return 35
    case let x where (35..<40).contains(x): return 40
    case let x where (40..<45).contains(x): return 45
    case let x where (45..<50).contains(x): return 50
    case let x where (50..<55).contains(x): return 55
    case let x where (55..<59).contains(x): return 59
    default: return 59
    }
        
}
