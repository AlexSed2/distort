// T1 x F = T2
// F = ?

/// - Returns: Edge
public func extrudeByZ<Scalar>(bottomEdge: (firstbottom: SIMD3<Scalar>, lastbottom: SIMD3<Scalar>), delta: Scalar) 
    -> (firstbottom: SIMD3<Scalar>, lastbottom: SIMD3<Scalar>, firsttop: SIMD3<Scalar>, lasttop: SIMD3<Scalar>)
    where Scalar: SIMDScalar, Scalar: FloatingPoint
{
    var vector = SIMD3<Scalar>()
    vector.z = delta
    let firsttop = bottomEdge.firstbottom + vector
    let lasttop = bottomEdge.lastbottom + vector
    return (bottomEdge.firstbottom, bottomEdge.lastbottom, firsttop, lasttop)
}

