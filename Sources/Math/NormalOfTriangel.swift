import simd


//public func getNormalOf<Scalar>(triangel verticies: [SIMD3<Scalar>]) -> SIMD3<Scalar>
//    where    
//    Scalar: SIMDScalar,
//    Scalar: FloatingPoint
//{
//    guard verticies.count == 3 else { return SIMD3<Scalar>([Scalar.zero, Scalar.zero, Scalar.zero])}
//    let vertex1 = SIMD3<Scalar>([verticies[0].x, verticies[0].y, verticies[0].z])
//    let vertex2 = SIMD3<Scalar>([verticies[1].x, verticies[1].y, verticies[1].z])
//    let vertex3 = SIMD3<Scalar>([verticies[2].x, verticies[2].y, verticies[2].z])
//    
//    let vector1 = vertex2 - vertex3
//    let vector2 = vertex2 - vertex1
//    
//    if let vectorOne = vector1 as? SIMD3<Float>, let vectorTwo = vector2 as? SIMD3<Float> {
//        let normal = simd_normalize(simd_cross(vectorOne, vectorTwo))
//        let s: SIMD3<Float> = normal
//        return (s as! SIMD3<Scalar>)
//    }
//    
//    return SIMD3<Scalar>([Scalar.zero, Scalar.zero, Scalar.zero])
//}

public func getNormalOf(triangel verticies: [SIMD3<Float>]) -> SIMD3<Float> {
    guard verticies.count == 3 else { return SIMD3<Float>.zero}
    let vertex1 = SIMD3<Float>([verticies[0].x, verticies[0].y, verticies[0].z])
    let vertex2 = SIMD3<Float>([verticies[1].x, verticies[1].y, verticies[1].z])
    let vertex3 = SIMD3<Float>([verticies[2].x, verticies[2].y, verticies[2].z])
    
    let vectorOne = vertex2 - vertex3
    let vectorTwo = vertex2 - vertex1
    let normal = simd_normalize(simd_cross(vectorOne, vectorTwo))
    
    return normal
}  
