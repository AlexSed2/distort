public struct Ray3D<Scalar: FloatingPoint & Comparable & SIMDScalar> {
    let origin: SIMD3<Scalar>
    let direction: SIMD3<Scalar>
    
    public init(origin: SIMD3<Scalar>, direction: SIMD3<Scalar>) {
        self.origin = origin
        self.direction = direction
    }
    
    public func intersectWithXY() -> SIMD3<Scalar> {
        var currentposition = origin
        while currentposition.z > .zero {
            currentposition += direction / 10 // WARNING BUG CANDIDATE
        }
        return currentposition
    }
}
