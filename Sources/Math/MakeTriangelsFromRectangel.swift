
public func makeTwoTriangelsFrom<Element> (edge:  
    (firstbottom: Element, 
     lastbottom: Element,  
     firsttop: Element,    
     lasttop: Element)) -> (bottom: [Element], top: [Element])
{
    let bottom = [edge.lastbottom, edge.firstbottom, edge.firsttop]
    let top = [edge.firsttop, edge.lasttop, edge.lastbottom]
    return (bottom, top)
}
