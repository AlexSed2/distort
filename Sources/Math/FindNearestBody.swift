import simd

public struct FindNearestBody {
    public init() {}
    public func find<Body: BodyableSIMD3Double>(items: Set<Body>, point: SIMD3<Double>) -> Body?  {
        guard items.isEmpty == false else { return nil }
        var dictionary: [Body: Double] = [:]
        for item in items {
            let distance = point - item.position
            let length = simd_length(distance)
            dictionary[item] = length
        }
        let sorted = dictionary.sorted { $0.value < $1.value }
        return sorted.first?.key
    }
}
