import Darwin

public func printOktetInDecimal<C: BidirectionalCollection>(c: C) {
    let r = c.reversed()
    var result = 0;
    for (index, item) in r.enumerated() {
        guard let d = Int("\(item)") else { print("error2"); return }
        guard (0...1).contains(d) else { print("error3"); return }
        let rank = Int(pow(2, Double(index)))
        result += rank * d
    }
    print("oktet: \(c), result: \(result)")
}

public func testOktet() {
    let z = "00001111"
    printOktetInDecimal(c: z)
}
