infix operator **

public protocol AnimateableProperties {
    associatedtype Property: FloatingPoint
    var toanimate: Property { get set }
}

public protocol AnimateablePropertiesSIMD2Float {
    var toanimateXY: SIMD2<Float> { get }
    var toanimateZ: SIMD2<Float> { get }
    mutating func setZAdjust(scale: Float) 
    mutating func setXYAdjust(scale: Float) 
}

public protocol BodyableSIMD3Float {
    var position: SIMD3<Float> { get set }
    var velocity: SIMD3<Float> { get set }
    var acceleration: SIMD3<Float> { get set }
    var radius: Float { get }
}

public protocol BodyableSIMD3Double {
    var acceleration: SIMD3<Double> { get }
    var position: SIMD3<Double> { get set }
    var velocity: SIMD3<Double> { get set }
    var radius: Double { get }
    mutating func evaluateAcceleration()
}

public protocol Bodyable: Hashable {
    associatedtype VectorType: Vecteric & TwoDimensional
    var position: VectorType { get set }
    var velocity: VectorType { get set }
    var accelration: VectorType { get set }
    var radius: VectorType.Measure { get }
}

public protocol Vecteric: SignedNumeric {
    associatedtype ScalarProduct: FloatingPoint
    static func / (left: Self, right: Self.Magnitude) -> Self
    static func * (left: Self, right: ScalarProduct) -> Self
    static func ** (left: Self, right: Self) -> ScalarProduct
}

public protocol TwoDimensional {
    associatedtype Measure: SignedNumeric, Comparable
    var xmesure: Measure { get set }
    var ymesure: Measure { get set }
    mutating func mirrorX()
    mutating func mirrorY()
}

public extension TwoDimensional {
    mutating func rotatedHalfPie() {
        let tempxmesure = xmesure
        let tempymesure = ymesure
        xmesure = -tempymesure
        ymesure = tempxmesure
    }
    mutating func rotatedNegativeHalfPie() {
        let tempxmesure = xmesure
        let tempymesure = ymesure
        xmesure = tempymesure
        ymesure = -tempxmesure
    }
    mutating func mirrorX() { xmesure = -xmesure }
    mutating func mirrorY() { ymesure = -ymesure }
}

public protocol ThreeDimensional: TwoDimensional {
    var zmesure: Measure { get set }
}
