/// Nearest Element

///  [1, 2, 3, 4, 5, 6, 7, 8, 9] ->  [1, 2, 3, 4], [5, 6, 7, 8] . last element is dropped
public func makeSeparateFourhes<C: Collection>(c: C) -> [C.SubSequence] {
    var outResult: [C.SubSequence] = []
    var work = c.dropFirst(0)
    var next = true
    outer: while next {
        let result = work.prefix(4)
        if result.isEmpty {
            next = false
            break outer
        }
        work = work.dropFirst(4)
        outResult.append(result)
    }
    return outResult.filter { $0.count == 4 }
}


///  [1, 2, 3, 4, 5] ->  [1,2] [2,3] [3,4]

public func makeClosePairs<C: Collection>(c: C) -> [C.SubSequence] {
    
    var out: [C.SubSequence] = []
    
    var index = c.startIndex
    while index < c.endIndex {
        let first = index
        let second = c.index(after: first)
        guard second < c.endIndex else { break }
        let slice = c[first...second]
        out.append(slice)
        index = second
    }
    
    return out
    
}

///  [1, 2, 3, 4, 5] ->  (1,2) (2,3) (3,4)

public func makeClosePairsTuples<C: Collection>(c: C) -> [(firstelement: C.Element, secondelemnt: C.Element)]  {
    
    var out: [(C.Element, C.Element)] = []
    
    var index = c.startIndex
    while index < c.endIndex {
        let first = index
        let second = c.index(after: first)
        guard second < c.endIndex else { break }
        let slice = c[first...second]
        guard slice.count == 2 else { break }
        let slicefirst = slice.startIndex
        let slicesecond = slice.index(after: slicefirst)
        out.append((slice[slicefirst], slice[slicesecond]))
        index = second
    }
    
    return out
    
}

/// [1, 2, 3, 4, 5, 6] -> [1, 2, 3] [4, 5, 6]

func sliceOnTwo<D: Collection>(collection: D) -> (left: D.SubSequence, right: D.SubSequence)
    where D.Index: Strideable, D.Index.Stride: SignedInteger
{
    let distance = collection.startIndex.distance(to: collection.endIndex)
    let middlecount = distance / 2
    let middle = collection.startIndex.advanced(by: middlecount)
    let firsthalf = collection[...middle]
    let secondhalf = collection[middle...]
    return (left: firsthalf, right: secondhalf)
}


/// [1, 2, 3, 4, 5, 6, 7, 8] -> [1, 2, 3, 4] [5, 6, 7, 8]

func sliceOnTwoEven<D: Collection>(collection: D) -> (left: D.SubSequence, right: D.SubSequence)
    where D.Index: Strideable, D.Index.Stride: SignedInteger
{
    let distance = collection.startIndex.distance(to: collection.endIndex)
    let middlecount = distance / 2
    let middle = collection.startIndex.advanced(by: middlecount)
    let firsthalf = collection[..<middle]
    let secondhalf = collection[middle...]
    return (left: firsthalf, right: secondhalf)
}

/// [1, 2, 3, 4, 5, 6] -> (1,2) (1,3) (1,3) (1,5) (1,6) (2,3) (2,4) (2,5) (2,6) ...

public func eachToEachIn<C: Collection>(c: C) -> [(left: C.Element, right: C.Element)]?  where C.Element: Hashable {
    guard c.count > 1 else { return nil }
    var rightindex = c.startIndex
    var leftindex = c.startIndex
    var controlSet = Set<CoupleUnorderer<C.Element>>()
    controlSet.reserveCapacity(c.count * (c.count - 1) / 2)
    while rightindex < c.endIndex {
        while leftindex < c.endIndex {
            let left = c[leftindex]
            let right = c[rightindex]
            if left != right {
                let tuple = CoupleUnorderer(left: left, right: right)
                controlSet.insert(tuple)
            }
            leftindex = c.index(after: leftindex)
            if leftindex == c.endIndex { break }
        }
        leftindex = c.startIndex
        rightindex = c.index(after: rightindex)
        if rightindex == c.endIndex { break }
    }
    return controlSet.map { $0.tuple }
}
