import Math


// Каждый вертекс должен сопроваждаться своей нормалью.
public func extrude<C: BidirectionalCollection>(profile: C, delta: Float) -> [(vertex: C.Element, normal: SIMD3<Float>)]
    where C.Element == SIMD3<Float> 
{
    var pairs = makeClosePairsTuples(c: profile)
    pairs.append((profile.last!, profile.first!))
    let faces = pairs.map { extrudeByZ(bottomEdge: $0, delta: delta) }
    let triangels = faces.map { makeTwoTriangelsFrom(edge: $0) }
    
    let verteciesAndNormals = triangels
        .reduce(into: [(vertex: C.Element, normal: SIMD3<Float>)]()) { result, tuple  in
            let bottomtuples = tuple.bottom.map { ($0, getNormalOf(triangel: tuple.bottom)) }
            let toptuples = tuple.top.map { ($0, getNormalOf(triangel: tuple.top)) }
            let toadd = bottomtuples + toptuples
            result += toadd
    }
    
    return verteciesAndNormals
}


//// Каждый вертекс должен сопроваждаться своей нормалью.
//public func extrude<C: BidirectionalCollection, Scalar>(profile: C, delta: Scalar) -> [(vertex: C.Element, normal: SIMD3<Scalar>)]
//    where C.Element == SIMD3<Scalar>, Scalar: FloatingPoint 
//{
//    var pairs = makeClosePairsTuples(c: profile)
//    pairs.append((profile.last!, profile.first!))
//    let faces = pairs.map { extrudeByZ(bottomEdge: $0, delta: delta) }
//    let triangels = faces.map { makeTwoTriangelsFrom(edge: $0) }
//    // 2 треугольника, 2 нормали
////    let verteciesAndNormals = triangels
////        .reduce([(vertex: C.Element, normal: SIMD3<Scalar>)]()) { (result, tuple) -> [(vertex: C.Element, normal: SIMD3<Scalar>)] in
////            let bottomtuples = tuple.bottom.map { ($0, getNormalOf(triangel: tuple.bottom)) }
////            let toptuples = tuple.top.map { ($0, getNormalOf(triangel: tuple.top)) }
////            let toadd = bottomtuples + toptuples
////            let out = result + toadd
////            return out
////    }
//    
//    
//    let verteciesAndNormals = triangels
//        .reduce(into: [(vertex: C.Element, normal: SIMD3<Scalar>)]()) { result, tuple  in
//            let bottomtuples = tuple.bottom.map { ($0, getNormalOf(triangel: tuple.bottom)) }
//            let toptuples = tuple.top.map { ($0, getNormalOf(triangel: tuple.top)) }
//            let toadd = bottomtuples + toptuples
//            result += toadd
//    }
//    
//    return verteciesAndNormals
//}

