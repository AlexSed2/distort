public extension Set {
    mutating func add<C: Collection>(collection: C) where C.Element == Element {
        for item in collection { insert(item) }
    }
}
