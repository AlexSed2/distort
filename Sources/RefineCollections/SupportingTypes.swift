public struct CoupleUnorderer<Element>: Equatable, Hashable where Element: Hashable {
    public let left: Element
    public let right: Element
    public init(left: Element, right: Element) {
        self.left = left
        self.right = right
    }
    var tuple: (left: Element, right: Element) { (left, right) }
    public static func == (lhs: CoupleUnorderer<Element>, rhs: CoupleUnorderer<Element>) -> Bool {
        let leftset: Set = [lhs.left, lhs.right]
        let rightset: Set = [rhs.left, rhs.right]
        return leftset == rightset
    }
    public func hash(into hasher: inout Hasher) {
        let set: Set = [left, right]
        hasher.combine(set)
    }
}
