
public struct CollectionAndTail {
    
    public init() {}
    
    public func checkSlice() {
        let z = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        let s = z[5...]
        print(s)
    }
    
    public func extractTail<C: RandomAccessCollection>(c: C, tailSize: Int) -> (cpllection: C, tail: C.SubSequence) {
        let collection = c
        let tail = c.suffix(tailSize)
        return (collection, tail)
    }
    
    public func stickTail<C: RangeReplaceableCollection & BidirectionalCollection>(incomc: C, tail: C) -> C 
        where C.Element: Comparable {
            guard incomc.isEmpty == false else { 
                return incomc 
            }
            guard tail.isEmpty == false else { 
                return incomc 
            }
            var workc = incomc
            let a = incomc.first!
            let b = incomc.last!
            let c = tail.first!
            let d = tail.last!
            if c >= b { // хвост сзади 
                workc.append(contentsOf: tail)
                return workc
            } 
            if a >= d { // хвост спереди 
                workc.insert(contentsOf: tail, at: workc.startIndex)
                return workc
            } 
            return incomc
    }
    
    public func test() {
        testEquailTail()
    }
    
    func testEquailTail() {
        let array = [4, 5, 6, 7, 8]
        let tail = [1, 2, 3]
        let appended = stickTail(incomc: array, tail: tail)
        print("appended: \(appended)")
    }
    
    func testPreview() {
        let array = [5, 6, 7, 8]
        let tail = [1, 2, 3]
        let appended = stickTail(incomc: array, tail: tail)
        print("appended: \(appended)")
    }
    
}
