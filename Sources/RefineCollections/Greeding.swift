import Math


public func makeDrawPairsFromGreedableCollection<C: Collection>(collection: C) -> [ArraySlice<C.Element>]?
    where 
    C.Element: TwoDimensional, 
    C.Element: Equatable, 
    C.SubSequence: RangeReplaceableCollection {
        guard let tuple = makeRawsAndColumnsFromGreed(collection: collection) else { return nil }
        let drawpairs = makeDrawPairs(raws: tuple.raws, columns: tuple.columns)
        return drawpairs
}

func makeDrawPairs<C: Collection, Z: Collection>(raws: C, columns: Z) -> [C.Element.SubSequence]
    where 
    C.Element: Collection, 
    Z.Element: Collection,
    C.Element.Element == Z.Element.Element,
    C.Element.SubSequence == Z.Element.SubSequence {
        var pairs = [C.Element.SubSequence]()
        for raw in raws {
            let rawpairs = makeClosePairs(c: raw)
            pairs.append(contentsOf: rawpairs)
        }
        for column in columns {
            let columnpairs = makeClosePairs(c: column)
            pairs.append(contentsOf: columnpairs)
        }
        return pairs
}

public func makeRawsAndColumnsFromGreed<C: Collection>(collection: C) -> (raws: [[C.Element]], columns: [[C.Element]])?    
    where 
    C.Element: TwoDimensional, 
    C.Element: Equatable, 
    C.SubSequence: RangeReplaceableCollection {
        guard let raws = makeRawsFromGreed(collection: collection) else { return nil }
        let normalraws = raws.map { Array($0) }
        guard let columns = makeColumnsFromRaws(collection: normalraws) else { return nil }
        return (normalraws, columns)
}


public func makeColumnsFromRaws<C: Collection>(collection: C) -> [[C.Element.Element]]?
    where C.Element: Collection {
        
        var counts = [Int]()
        for item in collection {
            counts.append(item.count)
        }
        guard counts.count > 0 else { return nil }
        for item in counts { guard item <= counts.first! else { return nil} }
        var columns = [[C.Element.Element]]()
        var currentIndex = collection.first!.startIndex
        while currentIndex < collection.first!.endIndex {
            var column = [C.Element.Element]()
            for raw in collection {
                // The getting index from Subsequence cause an error
                column.append(raw[currentIndex])
            }
            columns.append(column)
            currentIndex = collection.first!.index(after: currentIndex)
        }
        return columns
}


/// * (0,3)(1,3)(2,3)(3,3)
/// * (0,2)(1,2)(2,2)(3,2)  
/// * (0,1)(1,1)(2,1)(3,1)
/// * (0,0)(1,0)(2,0)(3,0) -> (0,0)(1,0)(2,0)(3,0), (0,1)(1,1)(2,1)(3,1) ... 

public func makeRawsFromGreed<C: Collection>(collection: C) -> [C.SubSequence]? 
    where 
    C.Element: TwoDimensional, 
    C.Element: Equatable, 
    C.SubSequence: RangeReplaceableCollection {
        
        guard collection.count > 1 else { 
            return nil 
        }
        let first = collection.first!
        var previousY = first.ymesure
        var corners = [C.Element]()
        
        for item in collection {
            let currentY = item.ymesure
            if currentY > previousY {
                corners.append(item)
            }
            previousY = currentY
        }
        
//        print("corners: \(corners.count)")
        
        guard corners.count > 1 else { 
            return nil 
        }
        
        var rest: C.SubSequence?
        var bunches = [C.SubSequence]()
        
        for corner in corners {
            
            let innersplit: [C.SubSequence]
            
            if let rest = rest {
                innersplit = rest.split(separator: corner, maxSplits: 1)
            } else {
                innersplit = collection.split(separator: corner, maxSplits: 1)
            }
            
            let bunch = innersplit.first!

            bunches.append(bunch)
            rest = innersplit.last!
        }
        if let rest = rest {
            bunches.append(rest)
        }
        
        var cornersiterator = corners.makeIterator()
        var outbunches = [C.SubSequence]()
        for bunch in bunches.dropFirst() {
            var workbunch = bunch
            if let nextcorner = cornersiterator.next() {
                workbunch.insert(nextcorner, at: bunch.startIndex)
            }
            outbunches.append(workbunch)
        }
        
        outbunches.insert(bunches.first!, at: outbunches.startIndex)
        return outbunches
}
