import Draw
import CoreGraphics

public struct Slider<C: Collection> where C.Index: Strideable {
    
    public let startindex: C.Index
    public let endIndex: C.Index
    public var current: C.Index { willSet { preview = current } }
    public var preview: C.Index
    public var carriage: C.Index.Stride // to remove
    
    public init(collection: C) {
        self.startindex = collection.startIndex
        self.endIndex = collection.endIndex
        self.current = collection.startIndex
        self.carriage = C.Index.Stride.zero
        self.preview = current
    }
    
    public var isEmpty: Bool { startindex == endIndex }
    
    public var distanceToEnd: C.Index.Stride {
        return current.distance(to: endIndex)
    }
    
    mutating public func advanceBy(number: C.Index.Stride) {
        let oldcurrent = current
        current = current.advanced(by: number)
        let distance = oldcurrent.distance(to: current)
        carriage += distance
        if current >= endIndex { 
            current = startindex 
            carriage = C.Index.Stride.zero
        }
    }
    
    
    mutating public func mooveFromeStartBy(number: C.Index.Stride) {
        let wholedistance = startindex.distance(to: endIndex)
        guard number > .zero, number < wholedistance else { return }
        current = startindex.advanced(by: number)
        carriage = startindex.distance(to: current)
        if current >= endIndex { 
            current = endIndex
            carriage = current.distance(to: startindex)
        }
        if current <= startindex {
            current = startindex
            carriage = .zero
        }
    }
}

extension Slider where C.Index.Stride: BinaryInteger {
    mutating public func mooveFromeStartRingBy(number: C.Index.Stride) {
        let wholedistance = startindex.distance(to: endIndex)
        let ringnumber = number % wholedistance
        current = startindex.advanced(by: ringnumber)
        carriage = startindex.distance(to: current)
    }
}

extension Slider: Drawable where C.Index.Stride: BinaryInteger {
    public func draw(into renderer: Renderer) {
        
        let darkenbrown = Color(red: 149*0.7/255, green: 98*0.7/255, blue: 57*0.7/255, alpha: 0.5)
        let doubledarkenbrown = Color(red: 149*0.7/255, green: 98*0.7/255, blue: 57*0.7/255, alpha: 0.5)
        
        let width = startindex.distance(to: endIndex)
        
        let rect = CGRect(x: 0, y: 0, width: Int(width), height: 100)
        renderer.draw(color: darkenbrown, in: rect)
        
        let carriagerect = CGRect(x: Int(carriage), y: 0, width: 5, height: 100)
        renderer.draw(color: doubledarkenbrown, in: carriagerect)
    }
}
