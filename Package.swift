// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Grid",
    platforms: [
        .macOS(.v10_15),
        .iOS(.v9),
    ],
    products: [
        .library(name: "Tattoo", targets: ["DistortPhoto"]),
        .library(name: "MathTools", targets: ["Math", "Grid", "RefineCollections"]),
    ],    
    dependencies: [],
    targets: [
        .target(
            name: "Draw",
            dependencies: []),
        .target(
            name: "Math",
            dependencies: []),
        .target(
            name: "Grid",
            dependencies: []),
        .target(
            name: "RefineCollections",
            dependencies: ["Math", "Draw"]),
        .target(
            name: "App",
            dependencies: ["Draw"]),
        .target(
            name: "Textures",
            dependencies: ["Draw", "Math"]),
        .target(
            name: "GridTools",
            dependencies: ["Math", "Draw", "Grid", "App", "Textures"]),
        .target(
            name: "DistortPhoto", 
            dependencies: ["App", "Draw", "Math", "GridTools"]),
        .target(
            name: "Runner",
            dependencies: ["GridTools", "DistortPhoto", "App"]),
    ]
)
