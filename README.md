# Grid

#### Набор модулей для реализации композиции стикера и фотографии.

Можно запустить на Mac, для этого скачайте репозиторий, на рабочем столе заведите папку TestImages, положите туда изображения back.jpg и fore.jpg, в ней заведите пустую папку Results, в терминале перейдите в папку проекта
- `$ cd Grid` 

и выполните команду 
- `$ swift run Runner` 

Теперь можно вводить команды и затем ввод, они находятся в файле Distort.swift  внизу.

###### © Nipple System Gatchina ® для InstaDev Saints-Petersburg ® 2021 - 2030 🇷🇺

